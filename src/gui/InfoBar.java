package gui;

import java.awt.Color;
import java.awt.Graphics2D;

import model.Drawable;
import model.Game;
import model.GameVars;

public class InfoBar extends GameMenu implements Drawable {

	public InfoBar(Game game) {
		super(game);
		String[] buttonStrings = GameVars.INFOBAR_OPTIONS;
		int[] commands = GameVars.INFOBAR_COMMANDS;
		int[] actions = GameVars.INFOBAR_BUTTON_ACTIONS;
		buttons = new GameButton[buttonStrings.length];
		buttons[0] = new TextButton(GameVars.DEFAULT_FRAME_WIDTH / 2, 11, 100,
				15, buttonStrings[0], commands[0], actions[0]);
		buttons[1] = new TextButton(600, 11, 25, 15, buttonStrings[1],
				commands[1], actions[1]);
		buttons[2] = new TextButton(660, 11, 25, 15, buttonStrings[2],
				commands[2], actions[2]);
		buttons[3] = new TextButton(500, 11, 100, 15, buttonStrings[3],
				commands[3], actions[3]);
	}

	@Override
	public void openNextMenu(String name) {
		if (name.equals("Pause Game"))
			nextMenu = new PauseMenu(game);
		else if (name.equals("Add Tower"))
			nextMenu = new TowerSelectionMenu(game);
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(GameVars.INFOBAR_COLOR);
		g.fillRect(0, 0, GameVars.DEFAULT_FRAME_WIDTH, GameVars.INFOBAR_HEIGHT);
		g.drawString(game.getTimeScale() + "x", 615, 15);
		g.setColor(Color.red);
		g.drawString("Wave Number: " + game.getLevel (), 70, 15);
		g.setColor(Color.yellow);
		g.drawString ("Formula: " + game.getUser().getCurrency(), 180, 15);
		super.draw(g);
	}

}
