package gui;

import java.awt.Graphics2D;

import model.Drawable;
import model.Game;
import model.GameVars;

public class PauseMenu extends PopupMenu implements Drawable {

	public PauseMenu(Game game) {
		super(GameVars.DEFAULT_FRAME_WIDTH / 2,
				GameVars.DEFAULT_FRAME_HEIGHT / 2, 200, 200, game);
		String[] buttonStrings = GameVars.PAUSEMENU_OPTIONS;
		int[] commands = GameVars.PAUSEMENU_COMMANDS;
		int[] actions = GameVars.PAUSEMENU_BUTTON_ACTIONS;
		autoLoadButtons(buttonStrings, commands, actions);
	}

	@Override
	public void openNextMenu(String name) {
		if (name.equals("Options"))
			nextMenu = new TowerSelectionMenu(game);
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(GameVars.PAUSEMENU_BACKGROUND_COLOR);
		g.fillRect(0, 0, GameVars.DEFAULT_FRAME_WIDTH,
				GameVars.DEFAULT_FRAME_HEIGHT);
		super.draw(g);
	}
}
