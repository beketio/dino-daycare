package gui;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import model.Drawable;
import model.Game;
import model.GameToolbox;
import model.GameVars;



public abstract class GameMenu implements Drawable {
	protected GameButton[] buttons;
	protected Game game;
	private BufferedImage background = GameToolbox.getImage("titlescreen.png");

	protected GameMenu nextMenu;

	public GameMenu(Game game) {
		this.game = game;
		buttons = new GameButton[0];
	}

	protected void centerButtons(int spacing) {
		if (buttons.length % 2 == 0) {
			for (int i = 0; i < buttons.length; i++) {
				buttons[i].moveTo(GameVars.DEFAULT_FRAME_WIDTH / 2,
						GameVars.DEFAULT_FRAME_HEIGHT / 2 + spacing / 2
								+ (i - buttons.length / 2) * spacing);
			}
		} else {
			for (int i = 0; i < buttons.length; i++) {
				buttons[i].moveTo(GameVars.DEFAULT_FRAME_WIDTH / 2,
						GameVars.DEFAULT_FRAME_HEIGHT / 2
								+ (i - buttons.length / 2) * spacing);
			}
		}
	}

	protected void autoLoadButtons(String[] buttonStrings, int[] commands,
			int[] actions) {
		buttons = new GameButton[buttonStrings.length];
		for (int i = 0; i < buttonStrings.length; i++)
			buttons[i] = new TextButton(GameVars.DEFAULT_FRAME_WIDTH / 2,
					GameVars.PAUSEMENU_BUTTON_STARTHEIGHT + i
							* GameVars.PAUSEMENU_BUTTON_DISTANCE,
					buttonStrings[i], commands[i], actions[i]);
		centerButtons(GameVars.PAUSEMENU_BUTTON_DISTANCE);
	}

	public void openNextMenu(String name) {

	}

	public void closeNextMenu() {
		nextMenu = null;
	}

	public void draw(Graphics2D g) {
		
		for (GameButton gb : buttons)
			gb.draw(g);
		if (nextMenu != null)
			nextMenu.draw(g);
	}

	public void mouseMoved(int x, int y) {
		if (nextMenu != null)
			nextMenu.mouseMoved(x, y);
		else
			for (GameButton gb : buttons)
				gb.mouseMoved(x, y);
	}

	public void mousePressed(int x, int y) {
		if (nextMenu != null)
			nextMenu.mousePressed(x, y);
		else
			for (GameButton gb : buttons)
				gb.mousePressed(x, y);
	}

	public int mouseReleased(int x, int y) {
		//int command = GameVars.COMMAND_DONOTHING;
		int action = GameVars.BUTTON_ACTION_DONOTHING;
		if (nextMenu != null)
			action = nextMenu.mouseReleased(x, y);
		else
			for (GameButton gb : buttons) {
				if (gb.isPressed(x, y)) {
					//command = gb.getCommand();
					gb.executeCommand(game);
					action = gb.getAction();
					if (action == GameVars.BUTTON_ACTION_NEXTMENU) {
						openNextMenu(gb.getName());
						gb.deselect();
						action = GameVars.BUTTON_ACTION_DONOTHING;
					}
				}
				gb.unPress();
			}
		switch (action) {
		case GameVars.BUTTON_ACTION_BACK_ONE_MENU:
			action = GameVars.BUTTON_ACTION_SET_TO_REMOVE;
			break;
		case GameVars.BUTTON_ACTION_SET_TO_REMOVE:
			action = GameVars.BUTTON_ACTION_DONOTHING;
		case GameVars.BUTTON_ACTION_RETURN_TO_ROOT:
			nextMenu = null;
			break;
		}
		//game.executeCommand(command);
		return action;
	}

}
