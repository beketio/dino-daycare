package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import model.Drawable;
import model.Game;
import model.GameToolbox;
import model.GameVars;

public class GamePanel extends JPanel {

	private Game game;

	private boolean render = true;
	private boolean caughtUp = true;

	public GamePanel(int width, int height) {
		setSize(width, height);
		setPreferredSize(new Dimension(width, height));
		game = new Game(width, height);
		setBackground(Color.WHITE);
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				game.mouseClicked(e.getX(), e.getY());
			}

			@Override
			public void mousePressed(MouseEvent e) {
				game.mousePressed(e.getX(), e.getY());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				game.mouseReleased(e.getX(), e.getY());
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
		this.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				game.mouseMoved(e.getX(), e.getY());
			}

			@Override
			public void mouseDragged(MouseEvent e) {
			}
		});
		new RenderThread().start();
	}

	public GamePanel(Game game) {
		this.game = game;
		setBackground(Color.WHITE);
		new RenderThread().start();
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		game.draw(g2);
		caughtUp = true;
	}

	public void close() {
		render = false;
	}

	private class RenderThread extends Thread {
		public void run() {
			while (render) {
				if (caughtUp) {
					caughtUp = false;
					repaint();
					try {
						sleep(GameVars.TARGET_FPS);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else
					try {
						sleep(2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
		}
	}

}
