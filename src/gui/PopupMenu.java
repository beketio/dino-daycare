package gui;

import java.awt.Graphics2D;

import model.Game;
import model.GameVars;

public abstract class PopupMenu extends GameMenu {

	protected GameButton background;

	public PopupMenu(int x, int y, int width, int height, Game game) {
		super(game);
		background = new BlankButton(x, y, width, height);
	}

	@Override
	public void draw(Graphics2D g) {
		background.draw(g);
		super.draw(g);
	}

}
