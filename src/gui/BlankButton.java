package gui;

import java.awt.Graphics2D;

public class BlankButton extends GameButton {

	public BlankButton(int x, int y, int width, int height) {
		super(x, y, width, height, "");
	}

	@Override
	protected void draw(Graphics2D g, int xdisp, int ydisp) {
	}

}
