package gui;

import model.Game;
import model.GameVars;

public class BoardTypeMenu extends PopupMenu {

	public BoardTypeMenu(Game game) {
		super(GameVars.DEFAULT_FRAME_WIDTH / 2,
				GameVars.DEFAULT_FRAME_HEIGHT / 2, 200, 200, game);
		String[] buttonStrings = GameVars.BOARD_OPTIONS;
		int[] commands = GameVars.BOARD_OPTIONS_COMMANDS;
		int[] actions = GameVars.BOARD_BUTTON_ACTIONS;
		autoLoadButtons(buttonStrings, commands, actions);
	}
}
