package gui;

import java.awt.FontMetrics;
import java.awt.Graphics2D;

import model.GameToolbox;
import model.GameVars;

public class TextButton extends GameButton {

	public TextButton(int x, int y, String name) {
		super(x, y, name);
	}

	public TextButton(int x, int y, String name, int command, int action) {
		super(x, y, name, command, action);
	}

	public TextButton(int x, int y, int width, int height, String name) {
		super(x, y, width, height, name);
	}

	public TextButton(int x, int y, int width, int height, String name,
			int command, int action) {
		super(x, y, width, height, name, command, action);
	}

	@Override
	protected void draw(Graphics2D g, int xdisp, int ydisp) {
		if (frame.getWidth() <= 0) {
			FontMetrics fm = g.getFontMetrics();
			int width = fm.stringWidth(name) + GameVars.BUTTON_BORDER_SIZE;
			int height = fm.getHeight() + GameVars.BUTTON_BORDER_SIZE / 2;
			frame.setRect(frame.getX() - width / 2, frame.getY() - height / 2,
					width, height);
		}
		FontMetrics fm = g.getFontMetrics();
		int textWidth = fm.stringWidth(name);
		g.setColor(GameVars.BUTTON_TEXT_COLOR);
		g.drawString(
				name,
				GameToolbox.roundDoubleToInt(frame.getCenterX() - textWidth / 2)
						+ xdisp,
				GameToolbox.roundDoubleToInt(frame.getCenterY()
						+ fm.getAscent() / 3)
						+ ydisp);

	}
}