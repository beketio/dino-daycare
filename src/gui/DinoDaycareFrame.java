package gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import model.GameVars;

public class DinoDaycareFrame extends JFrame {

	private GamePanel gamePanel;

	public static void main(String[] args) {
		new DinoDaycareFrame();
	}

	public DinoDaycareFrame() {
		// setSize(600, 600);
		setVisible(true);
		gamePanel = new GamePanel(GameVars.DEFAULT_FRAME_WIDTH,
				GameVars.DEFAULT_FRAME_HEIGHT);
		add(gamePanel);
		this.pack();
		// this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				gamePanel.close();
				System.exit(0);
			}
		});
	}
}
