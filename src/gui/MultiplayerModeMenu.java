package gui;

import model.Game;
import model.GameVars;

public class MultiplayerModeMenu extends PopupMenu {

	public MultiplayerModeMenu(Game game) {
		super(GameVars.DEFAULT_FRAME_WIDTH / 2,
				GameVars.DEFAULT_FRAME_HEIGHT / 2, 200, 200, game);
		String[] buttonStrings = GameVars.MULTIPLAYER_MODE_OPTIONS;
		int[] commands = GameVars.MULTIPLAYER_MODE_COMMANDS;
		int[] actions = GameVars.MULTIPLAYER_MODE_BUTTON_ACTIONS;
		autoLoadButtons(buttonStrings, commands, actions);
	}

}
