package gui;

import model.Game;
import model.GameToolbox;
import model.GameVars;

public class TowerSelectionMenu extends PopupMenu {

	public TowerSelectionMenu(Game game) {
		super(350, 350, 650, 650, game);
		String[] towers = GameToolbox.getTowerList();
		buttons = new GameButton[towers.length];
		int buttonWidth = 200;
		int buttonHeight = 150;
		for (int c = 0; c < 3; c++)
			for (int r = 0; r < 3; r++)
				buttons[c * 3 + r] = new TowerButton(125 + 650 / 3 * (r),
						125 + 650 / 3 * (c), buttonWidth, buttonHeight,
						towers[c * 3 + r], 0,
						GameVars.BUTTON_ACTION_BACK_ONE_MENU);
	}

}
