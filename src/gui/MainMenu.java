package gui;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import model.Drawable;
import model.Game;
import model.GameToolbox;
import model.GameVars;

public class MainMenu extends GameMenu implements Drawable {
	private BufferedImage background = GameToolbox.getImage("titlescreen.png");
	public MainMenu(Game game) {
		super(game);
		String[] buttonStrings = GameVars.MAINMENU_OPTIONS;
		int[] commands = GameVars.MAINMENU_COMMANDS;
		int[] actions = GameVars.MAINMENU_BUTTON_ACTIONS;
		autoLoadButtons(buttonStrings, commands, actions);
	}

	@Override
	public void openNextMenu(String name) {
		if (name.equals("Start Multiplayer"))
			nextMenu = new MultiplayerModeMenu(game);
		if (name.equals ("Start Singleplayer"))
		{
			nextMenu = new BoardTypeMenu (game);
		}
	}

	@Override
	public void draw(Graphics2D g) {
		g.drawImage(background, 0, 0, 700, 700, null);
		g.setColor(GameVars.PAUSEMENU_BACKGROUND_COLOR);
		g.fillRect(0, 0, GameVars.DEFAULT_FRAME_WIDTH,
				GameVars.DEFAULT_FRAME_HEIGHT);
		super.draw(g);
	}
}
