package gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import model.Game;
import model.GameToolbox;
import model.GameVars;

public class TowerButton extends GameButton {
	private BufferedImage img;
	private Rectangle imgFrame;
	private String[] stats = new String[8];
	private int longestString = 0;
	private int seperatorX = 0;

	public TowerButton(int x, int y, int width, int height, String tower,
			int command, int action) {
		super(x, y, width, height, tower, command, action);
		getStats(tower.toUpperCase());
	}

	private void fitText(FontMetrics font) {
		for (String s : stats) {
			int sWidth = font.stringWidth(s);
			if (sWidth > longestString)
				longestString = sWidth;
		}
		seperatorX = GameToolbox.roundDoubleToInt(frame.getWidth()
				- longestString - GameVars.BUTTON_BUFFER * 4);
		fitImage();
	}

	private void fitImage() {
		int imgWidth = img.getWidth();
		int imgHeight = img.getHeight();
		double ratio = imgWidth * 1.0 / imgHeight;
		System.out.println(seperatorX + "sepr");
		int displayWidth = GameToolbox.roundDoubleToInt(seperatorX
				- GameVars.BUTTON_BUFFER * 2);
		int displayHeight = GameToolbox.roundDoubleToInt(frame.getHeight()
				- GameVars.BUTTON_BUFFER * 2);
		System.out.println(displayWidth + " display dim " + displayHeight);
		if (imgWidth - displayWidth < imgHeight - displayHeight) {
			imgWidth = GameToolbox.roundDoubleToInt(displayHeight * ratio);
			imgFrame = new Rectangle(GameToolbox.roundDoubleToInt(frame.getX()
					+ GameVars.BUTTON_BUFFER + (displayWidth - imgWidth) / 2),
					GameToolbox.roundDoubleToInt(frame.getY()
							+ GameVars.BUTTON_BUFFER), imgWidth, displayHeight);
		} else {
			imgHeight = GameToolbox.roundDoubleToInt(displayWidth / ratio);
			System.out.println(imgHeight);
			imgFrame = new Rectangle(GameToolbox.roundDoubleToInt(frame.getX()
					+ GameVars.BUTTON_BUFFER),
					GameToolbox.roundDoubleToInt(frame.getY()
							+ GameVars.BUTTON_BUFFER
							+ (displayHeight - imgHeight) / 2), displayWidth,
					imgHeight);

		}
	}

	private void getStats(String className) {
		int rows = GameToolbox.getIntConstant(className,
				GameVars.VALUE_IMAGE_ROWS);
		int cols = GameToolbox.getIntConstant(className,
				GameVars.VALUE_IMAGE_COLS);
		if (rows == 1 && cols == 1)
			img = GameToolbox.getImage(GameToolbox.getStringConstant(className,
					GameVars.VALUE_IMAGE_LOCATION));
		else
			img = GameToolbox.getImagesFromSpritesheet(
					GameToolbox.getStringConstant(className,
							GameVars.VALUE_IMAGE_LOCATION), rows, cols)[0];
		stats[0] = GameToolbox
				.getStringConstant(className, GameVars.VALUE_NAME);
		stats[1] = GameVars.STRING_COST + ": "
				+ GameToolbox.getIntConstant(className, GameVars.VALUE_COST);
		stats[2] = GameVars.STRING_HEALTH
				+ ": "
				+ GameToolbox.getIntConstant(className,
						GameVars.VALUE_MAX_HEALTH);
		stats[3] = GameVars.STRING_ATTACK
				+ ": "
				+ GameToolbox.getIntConstant(className,
						GameVars.VALUE_ATTACK_POWER);
		stats[4] = GameVars.STRING_ATTACKINTERVAL
				+ ": "
				+ (1 / GameToolbox.getDoubleConstant(className,
						GameVars.VALUE_ATTACK_INTERVAL));
		stats[5] = GameVars.STRING_RANGE + ": "
				+ GameToolbox.getIntConstant(className, GameVars.VALUE_RANGE);
		stats[6] = GameVars.STRING_TURN_SPEED
				+ ": "
				+ GameToolbox.getDoubleConstant(className,
						GameVars.VALUE_ROTATION_SPEED);
		stats[7] = GameToolbox.getStringConstant(className,
				GameVars.VALUE_DESCRIPTION);
	}

	@Override
	public void executeCommand(Game game) {
		Object[] args = { name };
		game.executeCommand(GameVars.COMMAND_PLACE_TOWER, "", args);
	}

	@Override
	protected void draw(Graphics2D g, int xdisp, int ydisp) {
		FontMetrics fm = g.getFontMetrics();
		if (longestString == 0)
			fitText(g.getFontMetrics());
		g.drawImage(img, GameToolbox.roundDoubleToInt(imgFrame.getX()) + xdisp,
				GameToolbox.roundDoubleToInt(imgFrame.getY()) + ydisp,
				GameToolbox.roundDoubleToInt(imgFrame.getWidth()),
				GameToolbox.roundDoubleToInt(imgFrame.getHeight()), null);
		g.setColor(Color.BLACK);
		g.drawLine((int) frame.getX() + seperatorX + xdisp, (int) frame.getY()
				+ GameVars.BUTTON_BUFFER + ydisp, (int) frame.getX()
				+ seperatorX + xdisp,
				(int) (frame.getY() + frame.getHeight() + ydisp)
						- GameVars.BUTTON_BUFFER);
		int textHeight = fm.getHeight();
		int textX = GameToolbox.roundDoubleToInt(frame.getX() + seperatorX
				+ GameVars.BUTTON_BUFFER)
				+ xdisp;
		int textY = GameToolbox.roundDoubleToInt(frame.getY()
				+ GameVars.BUTTON_BUFFER)
				+ ydisp;
		g.drawString(stats[0], textX, textY + textHeight);
		g.drawLine(
				textX,
				textY + textHeight + 2,
				GameToolbox.roundDoubleToInt(frame.getX() + frame.getWidth()
						- GameVars.BUTTON_BUFFER * 2), textY + textHeight + 2);
		textY += GameVars.BUTTON_BUFFER;
		for (int i = 1; i < stats.length; i++) {
			g.drawString(stats[i], textX, textY + (i + 1) * textHeight);
		}
	}

}
