package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import model.Drawable;
import model.Game;
import model.GameToolbox;
import model.GameVars;

public abstract class GameButton implements Drawable {

	protected Rectangle frame;
	protected boolean selected = false, pressed = false;
	protected String name;
	protected int command = GameVars.COMMAND_DONOTHING;
	private int action = GameVars.BUTTON_ACTION_DONOTHING;
	private Color buttonColor = GameVars.BUTTON_COLOR,
			hoverColor = GameVars.BUTTON_HOVER_COLOR;

	public GameButton(int x, int y, String name) {
		frame = new Rectangle(x, y, 0, 0);
		this.name = name;
	}

	public GameButton(int x, int y, String name, int command, int action) {
		this(x, y, name);
		this.command = command;
		this.action = action;
	}

	public GameButton(int x, int y, int width, int height, String name) {
		frame = new Rectangle(x - width / 2, y - height / 2, width, height);
		this.name = name;
	}

	public GameButton(int x, int y, int width, int height, String name,
			int command, int action) {
		this(x, y, width, height, name);
		this.command = command;
		this.action = action;
	}

	public boolean contains(int x, int y) {
		return frame.contains(x, y);
	}

	public void moveTo(int x, int y) {
		frame.setLocation(x, y);
	}

	public void mouseMoved(int x, int y) {
		selected = contains(x, y);
		pressed = false;
	}

	public void mousePressed(int x, int y) {
		pressed = contains(x, y);
	}

	public void unPress() {
		pressed = false;
	}

	public void deselect() {
		selected = false;
	}

	public boolean isPressed(int x, int y) {
		return contains(x, y) && pressed;
	}

	public void executeCommand(Game game) {
		game.executeCommand(command);
	}

	public int getAction() {
		return action;
	}

	public void setButtonColor(Color buttonColor) {
		this.buttonColor = buttonColor;
	}

	public void setHoverColor(Color hoverColor) {
		this.hoverColor = hoverColor;
	}

	public String getName() {
		return name;
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(GameVars.BUTTON_SHADOW_COLOR);
		g.fillRoundRect(
				GameToolbox.roundDoubleToInt(frame.getX()
						+ GameVars.BUTTON_X_SHADOW_DISPLACEMENT),
				GameToolbox.roundDoubleToInt(frame.getY()
						+ GameVars.BUTTON_Y_SHADOW_DISPLACEMENT),
				GameToolbox.roundDoubleToInt(frame.getWidth()),
				GameToolbox.roundDoubleToInt(frame.getHeight()),
				GameVars.BUTTON_ARC_SIZE, GameVars.BUTTON_ARC_SIZE);
		if (selected)
			g.setColor(hoverColor);
		else
			g.setColor(buttonColor);
		int xdisp = 0, ydisp = 0;
		if (pressed) {
			xdisp = GameVars.BUTTON_X_SHADOW_DISPLACEMENT;
			if (xdisp > 0)
				xdisp--;
			else
				xdisp++;
			ydisp = GameVars.BUTTON_Y_SHADOW_DISPLACEMENT - 1;
		}
		g.fillRoundRect(GameToolbox.roundDoubleToInt(frame.getX() + xdisp),
				GameToolbox.roundDoubleToInt(frame.getY() + ydisp),
				GameToolbox.roundDoubleToInt(frame.getWidth()),
				GameToolbox.roundDoubleToInt(frame.getHeight()),
				GameVars.BUTTON_ARC_SIZE, GameVars.BUTTON_ARC_SIZE);
		draw(g, xdisp, ydisp);
	}

	protected abstract void draw(Graphics2D g, int xdisp, int ydisp);
}