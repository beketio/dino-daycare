package gui;

public class LabelButton extends TextButton {

	public LabelButton(int x, int y, String name) {
		super(x, y, name);
	}

	public LabelButton(int x, int y, int width, int height, String name,
			int command, int action) {
		super(x, y, width, height, name, command, action);
	}

	public void mouseMoved(int x, int y) {
	}

	public void mousePressed(int x, int y) {
	}

}
