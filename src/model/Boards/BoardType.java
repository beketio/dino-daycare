package model.Boards;

import java.util.Random;

public enum BoardType {
	playground, naproom, playroom;
	
	public String getBackgroundFilename(){
		if(this == playground){
			return "sand.png";
		}
		else return "carpet.png";
	}

	public String getNoneImage(){
		if(this == playground){
			return "quicksand.png";
		}
		if(this == playroom){
			return "jacks.png";
		}
		if(this == naproom){
			return "puke.png";
		}
		return null;
		
	}
	
	public String getTowerOnlyImage(){
		Random gen = new Random();
		int temp;
		if(this == playground){
			temp = gen.nextInt(2);
			if(temp == 0) return "swingset.png";
			else if (temp == 1) return "slide.png";
		}
		
		if(this == playroom){
			return "blockfort.png";
		}
		
		if(this == naproom){
			return "crib.png";
		}
		return null;
	}
	
	public String getEnemyOnlyImage(){
		if(this == playground){
			return "monkeybars.png";
		}
		
		if(this == playroom){
			return "timeout.png";
		}
		
		if(this == naproom){
			return "table.png";
		}
		
		return null;
	}
}
