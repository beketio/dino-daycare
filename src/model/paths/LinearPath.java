package model.paths;

import model.GameToolbox;
import model.gameObject.GameObject;
import model.gameObject.GamePath;

public class LinearPath implements GamePath {

	double rotation = 0;

	public LinearPath(double direction) {
		this.rotation = direction;
	}

	public LinearPath(double xpos, double ypos, GameObject tracked) {
		updatePath(xpos, ypos, tracked);
	}

	public void updatePath(double x, double y, GameObject tracked) {
		// double yV = (tracked.getYpos() - y);
		// double xV = (tracked.getXpos() - x);
		// rotation = Math.atan(xV / yV) + Math.PI / 2;
		// if ((xV > 0 && yV >= 0) || (xV < 0 && yV > 0))
		// rotation += Math.PI;
		rotation = GameToolbox.getAngle(x, y, tracked);
	}

	// @Override
	// public double getXVelocity(double x, double y, double speed) {
	// return xV * speed;
	// }
	//
	// @Override
	// public double getYVelocity(double x, double y, double speed) {
	// return yV * speed;
	// }

	@Override
	public void setDistance(double xpos, double ypos) {

	}

	@Override
	public double getRotation(double x, double y) {
		return rotation;
	}

}
