package model.paths;

import java.util.ArrayList;
import java.util.Collections;

import model.Board;
import model.GameToolbox;
import model.Tile;

public class PathFinder {
	private ArrayList<TileNode> closed = new ArrayList<TileNode>();
	private SortedTileNodeList open = new SortedTileNodeList();
	private Board board;
	private int maxSearchDistance;
	private TileNode[][] nodes;
	private boolean allowDiagMovement;

	public PathFinder(Board board, int maxSearchDistance,
			boolean allowDiagMovement) {
		this.board = board;
		this.maxSearchDistance = maxSearchDistance;
		this.allowDiagMovement = allowDiagMovement;
		nodes = new TileNode[board.getCols()][board.getRows()];
		for (int x = 0; x < board.getCols(); x++) {
			for (int y = 0; y < board.getRows(); y++)
				nodes[x][y] = new TileNode(board.getTile(y, x));
		}
	}

	public Tile[] findPath(Tile start, Tile end) {
		if (!end.canPass())
			return null;
		nodes[start.getCol()][start.getRow()].cost = 0;
		nodes[start.getCol()][start.getRow()].depth = 0;
		closed.clear();
		open.clear();
		open.add(nodes[start.getCol()][start.getRow()]);
		nodes[end.getCol()][end.getRow()].parent = null;
		int maxDepth = 0;
		while ((maxDepth < maxSearchDistance) && (open.size() != 0)) {
			TileNode current = getFirstInOpen();
			if (current == nodes[end.getCol()][end.getRow()])
				break;
			removeFromOpen(current);
			addToClosed(current);
			for (int x = -1; x < 2; x++) {
				for (int y = -1; y < 2; y++) {
					if ((x == 0) && (y == 0))
						continue;
					if (!allowDiagMovement)
						if ((x != 0) && (y != 0))
							continue;
					int xp = x + current.tile.getCol();
					int yp = y + current.tile.getRow();
					if (isValidLocation(xp, yp)) {
						TileNode neighbour = nodes[xp][yp];
						double nextStepCost = current.cost
								+ getCost(current.tile, neighbour.tile);
						if (nextStepCost < neighbour.cost) {
							if (inOpenList(neighbour))
								removeFromOpen(neighbour);
							if (inClosedList(neighbour))
								removeFromClosed(neighbour);
						}
						if (!inOpenList(neighbour)
								&& !(inClosedList(neighbour))) {
							neighbour.cost = nextStepCost;
							maxDepth = Math.max(maxDepth,
									neighbour.setParent(current));
							addToOpen(neighbour);
						}
					}
				}
			}
		}
		if (nodes[end.getCol()][end.getRow()].parent == null)
			return null;
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		TileNode target = nodes[end.getCol()][end.getRow()];
		while (target != nodes[start.getCol()][start.getRow()]) {
			tiles.add(0, target.tile);
			target = target.parent;
		}
		tiles.add(0, start);
		Tile[] tileArray = new Tile[tiles.size()];
		tileArray = tiles.toArray(tileArray);
		return tileArray;
	}

	private TileNode getFirstInOpen() {
		return open.first();
	}

	private void addToOpen(TileNode node) {
		open.add(node);
	}

	private boolean inOpenList(TileNode node) {
		return open.contains(node);
	}

	private void removeFromOpen(TileNode node) {
		open.remove(node);
	}

	private void addToClosed(TileNode node) {
		closed.add(node);
	}

	private boolean inClosedList(TileNode node) {
		return closed.contains(node);
	}

	private void removeFromClosed(TileNode node) {
		closed.remove(node);
	}

	private boolean isValidLocation(int x, int y) {
		if ((x < 0) || (y < 0) || (x >= board.getCols())
				|| (y >= board.getRows()))
			return false;
		Tile t = board.getTile(y, x);
		return t.canPass();
	}

	public double getCost(Tile t1, Tile t2) {
		return GameToolbox.getDistance(t1, t2);
	}

	private class SortedTileNodeList {
		private ArrayList<TileNode> list = new ArrayList<TileNode>();

		public TileNode first() {
			return list.get(0);
		}

		public void clear() {
			list.clear();
		}

		public void add(TileNode o) {
			list.add(o);
			Collections.sort(list);
		}

		public void remove(TileNode o) {
			list.remove(o);
		}

		public int size() {
			return list.size();
		}

		public boolean contains(TileNode node) {
			return list.contains(node);
		}
	}

	private class TileNode implements Comparable<TileNode> {
		private Tile tile;
		private double cost;
		private TileNode parent;
		private int depth;

		public TileNode(Tile tile) {
			this.tile = tile;
		}

		public int setParent(TileNode parent) {
			depth = parent.depth + 1;
			this.parent = parent;
			return depth;
		}

		public int compareTo(TileNode other) {
			if (cost < other.cost)
				return -1;
			if (cost > other.cost)
				return 1;
			return 0;
		}
	}
}