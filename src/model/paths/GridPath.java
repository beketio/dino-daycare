package model.paths;

import model.GameToolbox;
import model.Tile;
import model.gameObject.GamePath;

public class GridPath implements GamePath {

	public static final int PATH_ERROR = 10;

	private int[] xs, ys;
	private int next = 0;

	public GridPath(Tile[] tiles) {
		xs = new int[tiles.length];
		ys = new int[tiles.length];
		for (int i = 0; i < tiles.length; i++) {
			xs[i] = tiles[i].getX();
			ys[i] = tiles[i].getY();
		}
	}

	public GridPath(int[] xs, int ys[]) {
		if (xs.length != ys.length)
			System.out.println("LENGTH MISMATCH!!!");
		this.xs = xs;
		this.ys = ys;
	}

	@Override
	public double getRotation(double x, double y) {
		if (next >= xs.length)
			return 0;
		if (Math.abs(x - xs[next]) <= PATH_ERROR
				&& Math.abs(y - ys[next]) <= PATH_ERROR)
			next++;
		// Redundant code is redundant, can't think of a better way to do this
		if (next >= xs.length)
			return 0;
		return GameToolbox.getAngle(x, y, xs[next], ys[next]);
	}

	@Override
	public void setDistance(double xpos, double ypos) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < xs.length; i++)
			System.out.println(i + ": (" + xs[i] + ", " + ys[i] + ")\n");
		return s;
	}

}
