package model.paths;

import model.GameToolbox;
import model.gameObject.GameObject;
import model.gameObject.GamePath;

public class HomingPath implements GamePath {

	private GameObject tracked;
	private double lastRot = 0;

	public HomingPath(GameObject tracked) {
		this.tracked = tracked;
	}

	// @Override
	// public double getXVelocity(double x, double y, double speed) {
	// if (!tracked.isAlive())
	// return lastxV;
	// double angle = Math.atan2(y - tracked.getYpos(), x - tracked.getXpos());
	// double xV = -Math.cos(angle) * speed;
	// lastxV = xV;
	// return xV;
	// }

	// @Override
	// public double getYVelocity(double x, double y, double speed) {
	// if (!tracked.isAlive())
	// return lastyV;
	// double angle = Math.atan2(y - tracked.getYpos(), x - tracked.getXpos());
	// double yV = -Math.sin(angle) * speed;
	// lastyV = yV;
	// return yV;
	// }

	@Override
	public void setDistance(double xpos, double ypos) {
		// TODO Auto-generated method stub
	}

	@Override
	public double getRotation(double x, double y) {
		if (tracked == null)
			return lastRot;
		// double yV = (tracked.getYpos() - y);
		// double xV = (tracked.getXpos() - x);
		// double rotation = Math.atan(xV / yV) + Math.PI / 2;
		// if ((xV > 0 && yV >= 0) || (xV < 0 && yV > 0))
		// rotation += Math.PI;
		lastRot = GameToolbox.getAngle(x, y, tracked);
		return lastRot;
	}

}
