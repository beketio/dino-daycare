package model.paths;

import model.gameObject.GamePath;

public class OrbitingPath implements GamePath {

	private double centerx, centery, distance = 0;
	private boolean clockwise = true;

	public OrbitingPath(int centerx, int centery) {
		this.centerx = centerx;
		this.centery = centery;
	}

	public OrbitingPath(int centerx, int centery, boolean clockwise) {
		this.centerx = centerx;
		this.centery = centery;
		this.clockwise = clockwise;
	}

	@Override
	public void setDistance(double x, double y) {
		double a = x - centerx;
		double b = y - centery;
		distance = Math.sqrt(a * a + b * b);
	}

	//
	// @Override
	// public double getXVelocity(double x, double y, double speed) {
	// // double angle = Math.atan2(y - centery, x - centerx);
	// // return -Math.sin(angle) * speed;
	// return (centery - y) / distance * speed;
	// }
	//
	// @Override
	// public double getYVelocity(double x, double y, double speed) {
	// return (x - centerx) / distance * speed;
	// }

	@Override
	public double getRotation(double x, double y) {
		if (distance == 0)
			setDistance(x, y);
		double xV = (centery - y) / distance;
		double yV = (x - centerx) / distance;
		double rotation = Math.atan(xV / yV) + Math.PI / 2;
		if ((xV > 0 && yV >= 0) || (xV < 0 && yV > 0))
			rotation += Math.PI;
		if (!clockwise)
			rotation += Math.PI;

		return rotation;
	}
}
