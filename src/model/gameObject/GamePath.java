package model.gameObject;

public interface GamePath {

	public double getRotation(double x, double y);

	public void setDistance(double xpos, double ypos);

}
