package model.gameObject;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.GameToolbox;
import model.GameVars;
import model.Map;

public class testMovableObject extends MoveableGameObject {

	public testMovableObject(int x, int y, double scale, GamePath path, Map map) {
		super(x, y, map, path);
		setSpeed(150);
		setCurrentHealth(1000);
		setMaxHealth(1000);
		setRotation(path.getRotation(x, y));
		setImage(loadImage());
		setScale(0.3);
	}

	public BufferedImage loadImage() {
		return GameToolbox.getImage("test.png");
	}

}
