package model.gameObject;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.visual.Explosion;

public abstract class Projectile extends MoveableGameObject {

	protected GameObject origin;

	public Projectile(int x, int y, Map map, GamePath path, GameObject origin) {
		super(x, y, map, path);
		this.origin = origin;
		if (origin != null) {
			hardSetRotation(origin.getRotation());
			setAttackpower(origin.getAttackPower());
		}
	}

	@Override
	public void update(double seconds) {
		super.update(seconds);
		if (xpos + width / 2 < 0 || ypos + height / 2 < 0
				|| xpos + width / 2 > map.getWidth()
				|| ypos + height / 2 > map.getHeight())
			removeFromMap();
		GameObject[] gameObjects = map.getGameObjects();
		for (GameObject go : gameObjects)
			if (go instanceof Enemy)
				if (intersects(go)) {
					hit(go);
				}
	}

	public void hit(GameObject object) {
		object.takeDamage(getAttackPower());
		// new Explosion(GameToolbox.roundDoubleToInt(getXpos()),
		// GameToolbox.roundDoubleToInt(getYpos()), map);
		removeFromMap();
	}

}
