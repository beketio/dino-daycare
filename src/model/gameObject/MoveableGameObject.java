package model.gameObject;

import model.GameToolbox;
import model.GameVars;
import model.Map;
import model.visual.Explosion;

public abstract class MoveableGameObject extends GameObject {

	protected double xV, yV, speed;

	protected GamePath path;


	public MoveableGameObject(int x, int y, Map map, GamePath path) {
		super(x, y, map);
		if (path != null) {
			setPath(path);
			hardSetRotation(path.getRotation(x, y));
		}
	}

	protected void setStats() {
		super.setStats();
		setSpeed(GameToolbox.getDoubleConstant(this.getClass().getSimpleName(),
				GameVars.VALUE_SPEED));
	}

	public void setPath(GamePath path) {
		this.path = path;
	}

	public GamePath getPath() {
		return path;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public void update(double seconds) {
		// Explosion mode :D
		// new Explosion((int) xpos, (int) ypos, map).setScale(0.5);
		if (setToExplode)
			return;
		if (path != null) {
			setRotation(path.getRotation(xpos, ypos));
			super.update(seconds);
			yV = -Math.sin(rotation) * speed;
			xV = Math.cos(rotation) * speed;
		} else
			super.update(seconds);
		xpos += xV * seconds;
		ypos += yV * seconds;
		if (xpos > GameVars.DEFAULT_FRAME_WIDTH * 1.5) {
			explode();
			// removeFromMap();
		}
	}

	// Old update method, rotation based off velocity.
	/*
	 * @Override public void update(double seconds) { xpos += xV * seconds; ypos
	 * += yV * seconds; if (path != null) { xV = path.getXVelocity(xpos, ypos,
	 * speed); if (path != null) yV = path.getYVelocity(xpos, ypos, speed); }
	 * double rotation = -Math.atan(xV / yV) - Math.PI / 2; if ((xV > 0 && yV >=
	 * 0) || (xV < 0 && yV > 0)) rotation += Math.PI; setRotation(rotation); }
	 */

}