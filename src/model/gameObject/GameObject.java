package model.gameObject;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;

import model.Drawable;
import model.GameToolbox;
import model.GameVars;
import model.Map;
import model.Updateable;
import model.visual.Explosion;
import server.GameObjectPacket;

public abstract class GameObject implements Drawable, Updateable {

	private static int nextID = 0;
	private int iD = 0;

	protected Map map;

	protected int width, height;

	protected double xpos, ypos;
	protected double scale;

	public boolean isInvis = false;
	private BufferedImage image;
	private BufferedImage[] sprites;
	private float imgOpacity = 1f;
	private double spriteInterval;
	private double spriteIntervalCount = 0;
	private int spriteOn = 0;
	private int healthBarWidth = 0;
	protected int attackPower; // damage dealt by an attack
	protected double attackInterval; // number of secs between attacks
	protected int cost; // cost for level 1 purchase
	protected int currentHealth;
	protected int maxHealth;
	protected int range;
	protected boolean isAlive = true;
	protected String name;

	protected boolean setToExplode = false;

	protected double rotation = 0, targetRotation = rotation, rotationSpeed;

	public GameObject(int x, int y, Map map) {
		this.map = map;
		setXpos(x);
		setYpos(y);
		setStats();
		iD = nextID;
		nextID++;
		if (map != null)
			map.addGameObject(this);
	}

	public void setMap(Map map) {
		this.map = map;
		map.addGameObject(this);
	}

	public int getID() {
		return iD;
	}

	public void setID(int iD) {
		this.iD = iD;
		map.setID(this);
	}

	// Experimental
	protected void setStats() {
		String className = this.getClass().getSimpleName();
		setName(GameToolbox.getStringConstant(className, GameVars.VALUE_NAME));
		int rows = GameToolbox.getIntConstant(className,
				GameVars.VALUE_IMAGE_ROWS);
		int cols = GameToolbox.getIntConstant(className,
				GameVars.VALUE_IMAGE_COLS);
		if (rows == 1 && cols == 1)
			setImage(GameToolbox.getImage(GameToolbox.getStringConstant(
					className, GameVars.VALUE_IMAGE_LOCATION)));
		else
			setSprites(GameToolbox.getImagesFromSpritesheet(
					GameToolbox.getStringConstant(className,
							GameVars.VALUE_IMAGE_LOCATION), rows, cols));
		setScale(GameToolbox.getDoubleConstant(className, GameVars.VALUE_SCALE));
		setSpriteInterval(GameToolbox.getDoubleConstant(className,
				GameVars.VALUE_SPRITE_INTERVAL));
		setRotationSpeed(GameToolbox.getDoubleConstant(className,
				GameVars.VALUE_ROTATION_SPEED));
		setCost(GameToolbox.getIntConstant(className, GameVars.VALUE_COST));
		setMaxHealth(GameToolbox.getIntConstant(className,
				GameVars.VALUE_MAX_HEALTH));
		setRange(GameToolbox.getIntConstant(className, GameVars.VALUE_RANGE));
		setAttackpower(GameToolbox.getIntConstant(className,
				GameVars.VALUE_ATTACK_POWER));
		setAttackInterval(GameToolbox.getDoubleConstant(className,
				GameVars.VALUE_ATTACK_INTERVAL));
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		height = (int) (image.getHeight() * scale + 0.5);
		width = (int) (image.getWidth() * scale + 0.5);
	}

	public void setSprites(BufferedImage[] sprites) {
		this.sprites = sprites;
		setImage(sprites[0]);
		spriteIntervalCount = spriteInterval;
		spriteOn = 0;
	}

	public void setSprites(BufferedImage[] sprites, double spriteInterval) {
		setSprites(sprites);
		this.spriteInterval = spriteInterval;
	}

	public void setSpriteInterval(double interval) {
		spriteInterval = interval;
	}

	public void setOpacity(float opacity) {
		if (opacity >= 0)
			imgOpacity = opacity;
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getAttackPower() {
		return attackPower;
	}

	public void setAttackpower(int attackPower) {
		this.attackPower = attackPower;
	}

	public double getAttackInterval() {
		return attackInterval;
	}

	public void setAttackInterval(double attackRate) {
		this.attackInterval = attackRate;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
		currentHealth = maxHealth;
		healthBarWidth = GameToolbox.roundDoubleToInt(Math.log1p(currentHealth)
				* GameVars.HEALTH_BAR_RATIO);
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public double getXpos() {
		return xpos;
	}

	public void setXpos(double xpos) {
		this.xpos = xpos;
	}

	public double getYpos() {
		return ypos;
	}

	public void setYpos(double ypos) {
		this.ypos = ypos;
	}

	public void moveTo(double x, double y) {
		this.setXpos(x);
		this.setYpos(y);
	}

	public void setScale(double scale) {
		this.scale = scale;
		height = (int) (image.getHeight() * scale + 0.5);
		width = (int) (image.getWidth() * scale + 0.5);
	}

	public double getScale() {
		return scale;
	}

	public void removeFromMap() {
		map.removeGameObject(this);
		isAlive = false;
	}

	public void takeDamage(int damage) {
		if (!isAlive())
			return;
		currentHealth -= damage;
		if (currentHealth <= 0) {
			isAlive = false;
			explode();
		}

	}

	public boolean isAlive() {
		return isAlive;
	}

	public boolean intersects(GameObject otherObject) {
		// return getRect().intersects(otherObject.getRect());
		return getBounds().intersects(otherObject.getBounds().getBounds());
	}

	public Rectangle getRect() {
		return new Rectangle((int) (xpos - width / 2 + 0.5), (int) (ypos
				- height / 2 + 0.5), width, height);
	}

	public Area getBounds() {
		Area bounds = new Area(new Rectangle((int) (xpos - width / 4 + 0.5),
				(int) (ypos - height / 4 + 0.5), width / 2, height / 2));
		AffineTransform at = new AffineTransform();
		at.rotate(-rotation, xpos, ypos);
		return bounds.createTransformedArea(at);
	}

	public void setRotation(double targetRotation) {
		this.targetRotation = targetRotation % (Math.PI * 2);
	}

	public void hardSetRotation(double rotation) {
		this.rotation = rotation % (Math.PI * 2);
		targetRotation = this.rotation;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotationSpeed(double rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}

	public double getRotationSpeed() {
		return rotationSpeed;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void update(double seconds) {
		updateSprite(seconds);
		if (rotation != targetRotation) {
			double diff = rotation - targetRotation;
			if (Math.abs(diff) % (Math.PI * 2) < rotationSpeed * seconds)
				rotation = targetRotation;
			else if ((diff > 0 && Math.abs(diff) <= Math.PI)
					|| (diff < 0 && Math.abs(diff) > Math.PI))
				rotation -= rotationSpeed * seconds;
			else
				rotation += rotationSpeed * seconds;
		}
	}

	public void updateSprite(double seconds) {
		if (sprites != null) {
			if (spriteIntervalCount <= 0) {
				spriteIntervalCount = spriteInterval;
				spriteOn++;
				if (spriteOn == sprites.length)
					spriteOn = 0;
				image = sprites[spriteOn];
			}
			spriteIntervalCount -= seconds;
		}
	}

	@Override
	public void draw(Graphics2D g) {
		if (setToExplode)
			return;
		// g.draw(getBounds());
		if (image != null) {
			if (rotation != 0)
				g.setTransform(AffineTransform.getRotateInstance(-rotation,
						xpos, ypos));
			Composite originalComp = g.getComposite();
			if (imgOpacity < 1f) {
				g.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, imgOpacity));
			}
			g.drawImage(image, (int) ((xpos - width / 2) + 0.5), (int) (ypos
					- height / 2 + 0.5), width, height, null);
			if (imgOpacity < 1f)
				g.setComposite(originalComp);
			g.setTransform(AffineTransform.getRotateInstance(0));
		}

		if (maxHealth > 0) {
			int barHeight = (int) (GameVars.HEALTH_BAR_HEIGHT + 0.5);
			int barx = (int) (xpos - healthBarWidth / 2 + 0.5);
			int bary = (int) (ypos - GameVars.HEALTH_BAR_OFFSET + 0.5);
			int healthWidth = GameToolbox
					.roundDoubleToInt((maxHealth - currentHealth * 1.0)
							/ maxHealth * healthBarWidth);
			g.setColor(Color.GREEN);
			g.fillRect(barx, bary, healthBarWidth, barHeight);
			g.setColor(Color.ORANGE);
			g.fillRect(barx, bary, healthWidth, barHeight);
			g.setColor(Color.BLACK);
			g.drawRect(barx, bary, healthBarWidth, barHeight);
			// if (name != null)
			// g.drawString(name + " ID=" + iD, barx, bary - 10);
		}

	}

	public GameObjectPacket getObjectPacket() {
		return new GameObjectPacket(iD, GameToolbox.roundDoubleToInt(xpos),
				GameToolbox.roundDoubleToInt(ypos), rotation, currentHealth);
	}

	public void update(GameObjectPacket packet) {
		setXpos(packet.getX());
		setYpos(packet.getY());
		hardSetRotation(packet.getRotation());
		setCurrentHealth((int) packet.getHealth());
	}

	public void explode() {
		new Explosion((int) xpos, (int) ypos, map, this).setScale(0.3);
		setMaxHealth(0);
		setToExplode = true;
	}

	public String toString() {
		return "{Name=" + name + " ID=" + iD + " Class=" + this.getClass()
				+ "}";
	}
}
