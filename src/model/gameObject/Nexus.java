package model.gameObject;

import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

import model.GameVars;
import model.Map;
import model.Enemies.Enemy;
import model.Towers.Tower;

public class Nexus extends GameObject {

	public Nexus(int x, int y, Map m) {
		super(x, y, m);
		setMaxHealth(1000);
		attackPower = 0;
		attackInterval = 50;
		range = 0;
		name = "Nexus";
	}

	@Override
	public Area getBounds() {
		return new Area(new Rectangle((int) xpos, 0, 50,
				GameVars.DEFAULT_FRAME_HEIGHT));
	}
}
