package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import model.Boards.BoardType;
import model.gameObject.GameObject;
import model.paths.PathFinder;
import server.BoardInfo;

public class Board implements Drawable {

	private int x, y;
	private int width, height;
	private int rows, cols;
	private int rowWidth, colWidth;
	private BoardType type;

	private Tile[][] tiles;
	private Tile[] validStartTiles, validEndTiles;
	private ArrayList<Tile[]> paths;

	private PathFinder pathfinder;

	private Tile lastTile;
	private GameObject placeObject;
	private Rectangle availibilityFrame;
	private boolean available;

	public Board(int x, int y, int width, int height, int rows, int cols,
			BoardType type) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.rows = rows;
		this.cols = cols;
		this.type = type;
		rowWidth = (int) (height / rows + 0.5);
		colWidth = (int) (width / cols + 0.5);
		tiles = new Tile[rows][cols];
		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++)
				tiles[r][c] = new Tile(r, c, new Rectangle(x + c * colWidth, y
						+ r * rowWidth, colWidth, rowWidth), this);
		pathfinder = new PathFinder(this, 20, true);
		updateStartTiles();
		updateEndTiles();
		populateTiles();
	}

	public BoardType getBoardType() {
		return type;
	}

	public void drawAvailible(int x, int y, GameObject object) {
		Tile t = getTileFromPoint(x, y);
		if (t == null)
			clearAvailable();
		else if (t != lastTile) {
			available = addObject(t.getRow(), t.getCol(), object);
			if (available)
				t.removeObject();
			availibilityFrame = t.getFrame();
			placeObject = object;
			placeObject.moveTo(t.getX(), t.getY());
		}
	}

	public boolean hasAvailable() {
		return availibilityFrame != null;
	}

	public void clearAvailable() {
		availibilityFrame = null;
		placeObject = null;
	}

	private void populateTiles() {
		Random gen = new Random();
		int temp;
		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++) {
				temp = gen.nextInt(100) + 1;
				if (temp <= 5)
					setTile(r, c, TileType.none);
				else if (5 < temp && temp <= 15)
					setTile(r, c, TileType.enemyOnly);
				else if (15 < temp && temp <= 30)
					setTile(r, c, TileType.towerOnly);
				if (!checkPath())
					setTile(r, c, TileType.both);
			}
		populatePaths(getPaths());
	}

	private ArrayList<Tile[]> getPaths() {
		updateStartTiles();
		updateEndTiles();
		ArrayList<Tile[]> tempPaths = new ArrayList<Tile[]>();
		for (Tile st : validStartTiles)
			for (Tile et : validEndTiles) {
				Tile[] path = pathfinder.findPath(st, et);
				if (path != null)
					tempPaths.add(path);
			}
		return tempPaths;
	}

	private boolean populatePaths(ArrayList<Tile[]> tempPaths) {
		if (tempPaths.isEmpty())
			return false;
		paths = tempPaths;
		return true;
	}

	private boolean checkPath() {
		for (int s = 0; s < validStartTiles.length; s++)
			for (int e = 0; e < validEndTiles.length; e++) {
				Tile[] path = pathfinder.findPath(validStartTiles[s],
						validEndTiles[e]);
				if (path != null)
					return true;
			}
		return false;
	}

	private void updateStartTiles() {
		List<Tile> tileList = new ArrayList<Tile>();
		for (int i = 0; i < tiles[0].length; i++)
			if (tiles[i][0].canPass())
				tileList.add(tiles[i][0]);
		validStartTiles = tileList.toArray(new Tile[0]);
	}

	private void updateEndTiles() {
		List<Tile> tileList = new ArrayList<Tile>();
		for (int i = 0; i < tiles[0].length; i++)
			if (tiles[i][tiles.length - 1].canPass())
				tileList.add(tiles[i][tiles.length - 1]);
		validEndTiles = tileList.toArray(new Tile[0]);
	}

	private Tile[] getValidStartTiles() {
		return validStartTiles;
	}

	private Tile[] getValidEndTiles() {
		return validEndTiles;
	}

	public Tile[] getRandomPath() {
		int rand = (int) (Math.random() * paths.size());
		return paths.get(rand);
	}

	// public Tile getRandomStart() {
	// int rand = (int) (Math.random() * validStartTiles.length);
	// return validStartTiles[rand];
	// }
	//
	// public Tile getRandomEnd(Tile start) {
	// int rand = (int) (Math.random() * validEndTiles.length);
	// if (pathfinder.findPath(start, validEndTiles[rand]) == null)
	// return getRandomEnd(start);
	// return validEndTiles[rand];
	// }

	public void setTile(int row, int col, TileType type) {
		tiles[row][col].setTileType(type);
		// if (row == 0)
		// updateStartTiles();
		// if (row == rows - 1)
		// updateEndTiles();
	}

	public int getRows() {
		return tiles.length;
	}

	public int getCols() {
		return tiles[0].length;
	}

	// public Tile getStartTile() {
	// return tiles[tiles.length / 2][0];
	// }
	//
	// public Tile getEndTile() {
	// return tiles[tiles.length / 2][tiles.length - 1];
	// }

	public Tile getTile(int row, int col) {
		return tiles[row][col];
	}

	public Tile getTileFromPoint(int x, int y) {
		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++)
				if (tiles[r][c].contains(x, y))
					return tiles[r][c];
		return null;
	}

	public boolean addObject(int row, int col, GameObject object) {
		if (tiles[row][col].addObject(object)) {
			if (populatePaths(getPaths()))
				return true;
			tiles[row][col].removeObject();
			return false;
		}
		return false;
	}

	public void removeObject() {
		populatePaths(getPaths());
	}

	public BoardInfo getBoardInfo() {
		TileType[][] boardInfo = new TileType[tiles.length][tiles[0].length];
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < rows; c++) {
				boardInfo[r][c] = tiles[r][c].getTileType();
			}
		}
		return new BoardInfo(type, boardInfo);
	}

	public void setBoardInfo(BoardInfo info) {
		TileType[][] boardInfo = info.getInfo();
		BoardType bt = info.getBoardType();
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < rows; c++) {
				tiles[r][c].setBoardType(bt);
				tiles[r][c].setTileType(boardInfo[r][c]);
			}
		}
	}

	@Override
	public void draw(Graphics2D g) {
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {
				tiles[i][j].draw(g);
			}
		}
		if (availibilityFrame != null) {
			if (available)
				g.setColor(new Color(0, 255, 0, 200));
			else
				g.setColor(new Color(255, 0, 0, 200));
			g.fill(availibilityFrame);
			if (available)
				placeObject.draw(g);
		}
	}

}
