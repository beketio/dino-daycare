package model.visual;

import model.GameToolbox;
import model.Map;
import model.gameObject.GameObject;

public class Explosion extends model.gameObject.GameObject {

	private double timein = 0.2;
	private double timeout = 0.5;
	private boolean fadeIn = true;
	private double timeCounter = 0.1;

	private GameObject remove;

	// private int opacity = 255;

	public Explosion(int x, int y, Map map) {
		super(x, y, map);
		setImage(GameToolbox.getImage("explosion.png"));
		setScale(0.15);
		hardSetRotation(Math.random() * Math.PI * 2);
	}

	public Explosion(int x, int y, Map map, GameObject remove) {
		this(x, y, map);
		this.remove = remove;
	}

	public void updateSprite(double seconds) {
		super.updateSprite(seconds);
		if (fadeIn) {
			timeCounter += seconds;
			setOpacity((float) (timeCounter / timein));
			setScale(getScale() + 1 * seconds);
			if (timeCounter >= timein) {
				fadeIn = false;
				timeCounter = timeout;
				if (remove != null)
					remove.removeFromMap();
			}
		} else {
			timeCounter -= seconds;
			setOpacity((float) (timeCounter / timeout));
			if (timeCounter <= 0)
				removeFromMap();
		}

	}
}
