package model;

public interface Updateable {
	public void update(double seconds);
	public void updateSprite(double seconds);
}
