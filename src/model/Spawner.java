package model;

import java.util.Random;

import model.Enemies.IEDceratops;
import model.Enemies.MachineGunRaptor;
import model.Enemies.Pterodactyl;
import model.Enemies.Stealthadactyl;
import model.Enemies.Stegosaurus;
import model.Enemies.TRex;
import model.Enemies.TRocket;
import model.Enemies.Tankosaurus;
import model.Enemies.Triceritops;
import model.Enemies.Velociraptor;
import model.paths.GridPath;

public class Spawner implements Updateable {

	private Map map;
	private Board board;
	private double spawnInterval = 10;
	private double intervalCount = 5;
	private int[] xs, ys;

	private int[] tRexArray = { 1, 10 };
	private int[] IEDcerArray = { 11, 15 };
	private int[] MGRArray = { 16, 20 };
	private int[] pterArray = { 21, 30 };
	private int[] stealthArray = { 31, 35 };
	private int[] stegoArray = { 36, 45 };
	private int[] TankoArray = { 46, 50 };
	private int[] tricerArray = { 51, 60 };
	private int[] tRocketArray = { 61, 70 };
	private int[] veloArray = { 71, 100 };

	private int levelNumber = 1;
	private int spawnAmount = 10;
	private int spawnCounter = 0;
	private boolean timeBetween = false;

	public Spawner(Map map, Board board) {
		this.map = map;
		this.board = board;
	}

	@Override
	public void update(double seconds) {
		if (spawnCounter >= spawnAmount) {
			levelNumber++;
			spawnAmount += 5;
			timeBetween = true;
			intervalCount = 8;

			switch (levelNumber) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				GameVars.ENEMY_TRICERITOPS_SPEED += 5;
				GameVars.ENEMY_TREX_SPEED += 10;
			case 4:
				spawnInterval = 3;
			default:
				GameVars.ENEMY_TRICERITOPS_SPEED += 1;
				GameVars.ENEMY_TREX_SPEED += 5;
				GameVars.ENEMY_IEDCERATOPS_SPEED += 1;
				GameVars.ENEMY_MACHINEGUNRAPTOR_SPEED += 5;
				GameVars.ENEMY_STEGOSAURUS_SPEED += 1;
				GameVars.ENEMY_TANKOSAURUS_SPEED += 1;
				GameVars.ENEMY_TROCKET_SPEED += 5;
				GameVars.ENEMY_VELOCIRAPTOR_SPEED += 5;

				GameVars.ENEMY_TRICERITOPS_MAX_HEALTH += 10;
				GameVars.ENEMY_TREX_MAX_HEALTH += 10;
				GameVars.ENEMY_IEDCERATOPS_MAX_HEALTH += 15;
				GameVars.ENEMY_MACHINEGUNRAPTOR_MAX_HEALTH += 5;
				GameVars.ENEMY_STEGOSAURUS_MAX_HEALTH += 15;
				GameVars.ENEMY_TANKOSAURUS_MAX_HEALTH += 20;
				GameVars.ENEMY_TROCKET_MAX_HEALTH += 10;
				GameVars.ENEMY_VELOCIRAPTOR_MAX_HEALTH += 15;

			}

		}
		// seconds in time in seconds since last update (usually 0.01, but can
		// be lower is update thread is lagging)
		if (intervalCount <= 0) {
			if (timeBetween == true) {
				intervalCount = spawnInterval;
				timeBetween = false;
			} else {
				spawnEnemy();
				intervalCount = spawnInterval;
				spawnCounter++;
			}
		}
		intervalCount -= seconds;
	}

	public int getLevel() {
		return levelNumber;
	}

	private void spawnEnemy() {
		// Tile start = board.getRandomStart();
		// Tile end = board.getRandomEnd(start);
		// System.out.println("Start=" + start);
		// System.out.println("End=" + end);
		Tile[] path = board.getRandomPath();
		// new TRex(-50, path[0].getY(), map, new GridPath(path));
		// new Pterodactyl(-50, path[0].getY(), map, new GridPath(path));
		// // new Triceritops(0, 300, map, getRandomPath());
		// // new Stealthadactyl(0, 300, map, getRandomPath());

		if (timeBetween == false) {
			Random rn = new Random();
			int randomNum = rn.nextInt((100 - 1) + 1) + 1;
			new TRex(-50, path[0].getY(), map, new GridPath(path));

			if (randomNum >= tRexArray[0] && randomNum <= tRexArray[1]) {
				new TRex(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum > pterArray[0] && randomNum <= pterArray[1]) {
				new Pterodactyl(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= stealthArray[0] && randomNum <= stealthArray[1]) {
				new Stealthadactyl(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= IEDcerArray[0] && randomNum <= IEDcerArray[1]) {
				new IEDceratops(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= MGRArray[0] && randomNum <= MGRArray[1]) {
				new MachineGunRaptor(-50, path[0].getY(), map, new GridPath(
						path));
			}
			if (randomNum >= stegoArray[0] && randomNum <= stegoArray[1]) {
				new Stegosaurus(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= TankoArray[0] && randomNum <= TankoArray[1]) {
				new Tankosaurus(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= tricerArray[0] && randomNum <= tricerArray[1]) {
				new Triceritops(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= tRocketArray[0] && randomNum <= tRocketArray[1]) {
				new TRocket(-50, path[0].getY(), map, new GridPath(path));
			}
			if (randomNum >= veloArray[0] && randomNum <= veloArray[1]) {
				new Velociraptor(-50, path[0].getY(), map, new GridPath(path));
			}
		}
	}

	@Override
	public void updateSprite(double seconds) {
		// TODO Auto-generated method stub

	}

}
