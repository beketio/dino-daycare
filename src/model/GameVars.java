package model;

import java.awt.Color;

public class GameVars {

	/* ----------------------- */
	/* ---General Variables--- */
	/* ----------------------- */

	public static final int FPS_UPDATE_INTERVAL = 1000;
	public static final int TARGET_FPS = 16;

	public static final int DEFAULT_FRAME_WIDTH = 700;
	public static final int DEFAULT_FRAME_HEIGHT = 700;

	public static final long TARGET_UPDATE_PERIOD = 10;

	public static final String IMAGE_DIRECTORY = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "images"
			+ System.getProperty("file.separator");

	/* -------------------- */
	/* ---Game Variables--- */
	/* -------------------- */

	// Board Variables
	public static final int BOARD_DEFAULT_ROWS = 10;
	public static final int BOARD_DEFAULT_COLS = 10;
	public static final int BOARD_X_OFFSET = 50;
	public static final int BOARD_Y_OFFSET = 50;

	// Timescale Variables
	public static final int TIMESCALE_DEFAULT = 3;
	public static final double[] TIMESCALE_PRESETS = { 0.1, 0.25, 0.5, 1, 1.5,
			2, 4, 6 };

	// Healthbar Variables
	public static final int HEALTH_BAR_HEIGHT = 5;
	public static final int HEALTH_BAR_OFFSET = 25;
	public static final int HEALTH_BAR_RATIO = 10;

	/* ------------------------ */
	/* ---GameMode Variables--- */
	/* ------------------------ */

	public static final int GAMEMODE_MAINMENU = 0;
	public static final int GAMEMODE_MENU_SETTINGS = 1;
	public static final int GAMEMODE_MULTIPLAYER_MODE_SELECTION = 2;
	public static final int GAMEMODE_MULTIPLAYER_SETTINGS = 3;
	public static final int GAMEMODE_INGAME = 4;
	public static final int GAMEMODE_TOWER_SELECTION = 5;
	public static final int GAMEMODE_TOWERPLACEMENT = 6;
	public static final int GAMEMODE_PAUSEMENU = 7;
	public static final int GAMEMODE_INGAME_SETTINGS = 8;

	/* ----------------------- */
	/* ---Command Variables--- */
	/* ----------------------- */

	// GUI and Menu control commands
	public static final int COMMAND_DONOTHING = 0;
	public static final int COMMAND_START_GAME = 1;
	public static final int COMMAND_QUIT_GAME = 2;
	public static final int COMMAND_START_EASY = 3;
	public static final int COMMAND_START_MED = 18;
	public static final int COMMAND_START_HARD = 19;

	public static final int COMMAND_HOST_MULTI = 4;
	public static final int COMMAND_JOIN_MULTI = 5;
	public static final int COMMAND_OPTIONS = 6;

	// Game control commands
	public static final int COMMAND_PAUSE = 7;
	public static final int COMMAND_RESUME = 8;
	public static final int COMMAND_INCREASE_TIMESCALE = 9;
	public static final int COMMAND_DECREASE_TIMESCALE = 10;
	public static final int COMMAND_PLACE_TOWER = 11;

	// Server commands
	public static final int COMMAND_ADD_OBJECT = 12;
	public static final int COMMAND_REMOVE_OBJECT = 13;
	public static final int COMMAND_SET_BOARD = 14;
	public static final int COMMAND_SET_NAME = 15;
	public static final int COMMAND_SEND_MONEY = 16;
	public static final int COMMAND_SEND_MESSAGE = 17;

	/* ------------------- */
	/* ---GUI Variables--- */
	/* ------------------- */

	// Button Variables
	public static final int BUTTON_ARC_SIZE = 15;
	public static final int BUTTON_BORDER_SIZE = 30;
	public static final int BUTTON_X_SHADOW_DISPLACEMENT = -2;
	public static final int BUTTON_Y_SHADOW_DISPLACEMENT = 5;
	public static final Color BUTTON_COLOR = new Color(255, 255, 255, 200);
	public static final Color BUTTON_SHADOW_COLOR = new Color(0, 0, 0, 150);
	public static final Color BUTTON_HOVER_COLOR = new Color(150, 150, 255, 200);
	public static final Color BUTTON_TEXT_COLOR = new Color(0, 0, 0);
	public static final int BUTTON_BUFFER = 5;
	public static final int BUTTON_ACTION_DONOTHING = 0;
	public static final int BUTTON_ACTION_NEXTMENU = 1;
	public static final int BUTTON_ACTION_BACK_ONE_MENU = 2;
	public static final int BUTTON_ACTION_RETURN_TO_ROOT = 3;
	public static final int BUTTON_ACTION_SET_TO_REMOVE = 4;

	/* -------------------- */
	/* ---Menu Variables--- */
	/* -------------------- */

	// Infobar Vars
	public static final String[] INFOBAR_OPTIONS = { "Pause Game", "+", "-",
			"Add Tower" };
	public static final int[] INFOBAR_COMMANDS = { COMMAND_PAUSE,
			COMMAND_INCREASE_TIMESCALE, COMMAND_DECREASE_TIMESCALE,
			COMMAND_DONOTHING };
	public static final int[] INFOBAR_BUTTON_ACTIONS = {
			BUTTON_ACTION_NEXTMENU, BUTTON_ACTION_DONOTHING,
			BUTTON_ACTION_DONOTHING, BUTTON_ACTION_NEXTMENU };
	public static final int INFOBAR_HEIGHT = 25;
	public static final Color INFOBAR_COLOR = new Color(0, 0, 0, 150);

	// Main Menu Vars
	public static final String[] BOARD_OPTIONS = { "Playground", "Play Room",
			"Nap Room" };
	public static final int[] BOARD_OPTIONS_COMMANDS = { COMMAND_START_EASY,
			COMMAND_START_MED, COMMAND_START_HARD };
	public static final int[] BOARD_BUTTON_ACTIONS = { BUTTON_ACTION_NEXTMENU,
			BUTTON_ACTION_NEXTMENU, BUTTON_ACTION_NEXTMENU };

	public static final String[] MAINMENU_OPTIONS = { "Start Singleplayer",
			"Start Multiplayer", "Options" };
	public static final int[] MAINMENU_COMMANDS = { COMMAND_DONOTHING,
			COMMAND_DONOTHING, COMMAND_OPTIONS };
	public static final int[] MAINMENU_BUTTON_ACTIONS = {
			BUTTON_ACTION_NEXTMENU, BUTTON_ACTION_NEXTMENU,
			BUTTON_ACTION_NEXTMENU };

	// Main Menu Vars
	public static final String GAME_SETUP_TITLE = "Game Setup";
	public static final String[] GAME_SETUP_OPTIONS = { "Future Option" };
	public static final int[] GAME_SETUP_COMMANDS = { COMMAND_DONOTHING };
	public static final int[] GAME_SETUP_BUTTON_ACTIONS = { BUTTON_ACTION_BACK_ONE_MENU };

	// Pause Menu Vars
	public static final String PAUSEMENU_TITLE = "Game Paused";
	public static final String[] PAUSEMENU_OPTIONS = { "Resume Game",
			"Options", "Return to Main Menu" };
	public static final int[] PAUSEMENU_COMMANDS = { COMMAND_RESUME,
			COMMAND_OPTIONS, COMMAND_QUIT_GAME };
	public static final int[] PAUSEMENU_BUTTON_ACTIONS = {
			BUTTON_ACTION_BACK_ONE_MENU, BUTTON_ACTION_NEXTMENU,
			BUTTON_ACTION_DONOTHING };
	public static final Color PAUSEMENU_BACKGROUND_COLOR = new Color(255, 255,
			255, 160);
	public static final int PAUSEMENU_BUTTON_STARTHEIGHT = 300;
	public static final int PAUSEMENU_BUTTON_DISTANCE = 65;

	// Menu Settings Vars
	public static final String MENU_SETTINGS_TITLE = "Settings";
	public static final String[] MENU_SETTINGS_OPTIONS = { "Future Option" };
	public static final int[] MENU_SETTINGS_COMMANDS = { COMMAND_DONOTHING };
	public static final int[] MENU_SETTINGS_BUTTON_ACTIONS = { BUTTON_ACTION_BACK_ONE_MENU };

	// Ingame Settings Vars
	public static final String INGAME_SETTINGS_TITLE = "Settings";
	public static final String[] INGAME_SETTINGS_OPTIONS = { "Future Option" };
	public static final int[] INGAME_SETTINGS_COMMANDS = { COMMAND_DONOTHING };
	public static final int[] INGAME_SETTINGS_BUTTON_ACTIONS = { BUTTON_ACTION_BACK_ONE_MENU };

	// Confirmbox Vars
	public static final String[] COMFIRM_BOX_OPTIONS = { "Yes", "No" };
	public static final int[] CONFIRM_BOX_COMMANDS = { COMMAND_DONOTHING,
			COMMAND_DONOTHING };
	public static final int[] CONFIRM_BOX_BUTTON_ACTIONS = {
			BUTTON_ACTION_RETURN_TO_ROOT, BUTTON_ACTION_BACK_ONE_MENU };

	// Multiplayer Mode Selection Vars
	public static final String[] MULTIPLAYER_MODE_OPTIONS = { "Host Game",
			"Join Game", "Back" };
	public static final int[] MULTIPLAYER_MODE_COMMANDS = { COMMAND_HOST_MULTI,
			COMMAND_JOIN_MULTI, COMMAND_DONOTHING };
	public static final int[] MULTIPLAYER_MODE_BUTTON_ACTIONS = {
			BUTTON_ACTION_DONOTHING, BUTTON_ACTION_DONOTHING,
			BUTTON_ACTION_BACK_ONE_MENU };

	// Multiplayer Server Settings Vars
	public static final String MULTIPLAYER_HOST_TITLE = "Multiplayer Server Settings";
	public static final String[] MULTIPLAYER_HOST_OPTIONS = { "Future Option" };
	public static final int[] MULTIPLAYER_HOST_COMMANDS = { COMMAND_DONOTHING };
	public static final int[] MULTIPLAYER_HOST_BUTTON_ACTIONS = { BUTTON_ACTION_BACK_ONE_MENU };

	// Multiplayer Connection Settings Vars
	public static final String MULTIPLAYER_JOIN_TITLE = "Connection Settings";
	public static final String[] MULTIPLAYER_JOIN_OPTIONS = { "Future Option" };
	public static final int[] MULTIPLAYER_JOIN_COMMANDS = { COMMAND_DONOTHING };
	public static final int[] MULTIPLAYER_JOIN_BUTTON_ACTIONS = { BUTTON_ACTION_BACK_ONE_MENU };

	/* ------------------------- */
	/* ---Variable Value Keys--- */
	/* ------------------------- */

	// Value Keys
	public static final String VALUE_NAME = "_NAME";
	public static final String VALUE_DESCRIPTION = "_DESCRIPTION";
	public static final String VALUE_IMAGE_LOCATION = "_IMAGE_LOCATION";
	public static final String VALUE_IMAGE_ROWS = "_IMAGE_ROWS";
	public static final String VALUE_IMAGE_COLS = "_IMAGE_COLS";
	public static final String VALUE_ATTACK_POWER = "_ATTACK_POWER";
	public static final String VALUE_COST = "_COST";
	public static final String VALUE_MAX_HEALTH = "_MAX_HEALTH";
	public static final String VALUE_RANGE = "_RANGE";
	public static final String VALUE_SCALE = "_SCALE";
	public static final String VALUE_SPRITE_INTERVAL = "_SPRITE_INTERVAL";
	public static final String VALUE_ATTACK_INTERVAL = "_ATTACK_INTERVAL";
	public static final String VALUE_ROTATION_SPEED = "_ROTATION_SPEED";
	public static final String VALUE_SPEED = "_SPEED";

	// Value String Names
	public static final String STRING_ATTACK = "Attack";
	public static final String STRING_COST = "Cost";
	public static final String STRING_HEALTH = "Health";
	public static final String STRING_RANGE = "Range";
	public static final String STRING_ATTACKINTERVAL = "Attack Rate";
	public static final String STRING_TURN_SPEED = "Turn Speed";

	/* ----------------------- */
	/* ---Default Variables--- */
	/* ----------------------- */

	public static final String DEFAULT_NAME = "???";
	public static final String DEFAULT_DESCRIPTION = "This object does not have a description.";
	public static final String DEFAULT_IMAGE_LOCATION = "notFound.gif";
	public static final int DEFAULT_IMAGE_ROWS = 1;
	public static final int DEFAULT_IMAGE_COLS = 1;
	public static final int DEFAULT_ATTACK_POWER = 50;
	public static final int DEFAULT_COST = 100;
	public static final int DEFAULT_MAX_HEALTH = 0;
	public static final int DEFAULT_RANGE = 100;
	public static final double DEFAULT_SCALE = 1;
	public static final double DEFAULT_SPRITE_INTERVAL = 0.1;
	public static final double DEFAULT_ATTACK_INTERVAL = 1.0;
	public static final double DEFAULT_ROTATION_SPEED = 3;
	public static final double DEFAULT_SPEED = 100;

	/* --------------------- */
	/* ---Tower Variables--- */
	/* --------------------- */

	// BlockBombarder Tower Vars
	public static final String TOWER_BLOCKBOMBARDER_NAME = "Block Bombarder";
	public static final String TOWER_BLOCKBOMBARDER_DESCRIPTION = "It shoots blocks.";
	public static final String TOWER_BLOCKBOMBARDER_IMAGE_LOCATION = "BlockBabyWalkSprite.png";
	public static final int TOWER_BLOCKBOMBARDER_IMAGE_COLS = 2;
	public static final int TOWER_BLOCKBOMBARDER_ATTACK_POWER = 50;
	public static final int TOWER_BLOCKBOMBARDER_COST = 100;
	public static final int TOWER_BLOCKBOMBARDER_MAX_HEALTH = 500;
	public static final int TOWER_BLOCKBOMBARDER_RANGE = 200;
	public static final double TOWER_BLOCKBOMBARDER_ATTACK_INTERVAL = 2.0;
	public static final double TOWER_BLOCKBOMBARDER_SCALE = 0.9;
	public static final double TOWER_BLOCKBOMBARDER_ROTATION_SPEED = 6;

	// DiaperBomber Tower Vars
	public static final String TOWER_DIAPERBOMBER_NAME = "Diaper Bomber";
	public static final String TOWER_DIAPERBOMBER_DESCRIPTION = "It throws dirty diapers.";
	public static final String TOWER_DIAPERBOMBER_IMAGE_LOCATION = "ChangingTableTowerSprite.png";
	public static final int TOWER_DIAPERBOMBER_IMAGE_COLS = 2;
	public static final int TOWER_DIAPERBOMBER_ATTACK_POWER = 75;
	public static final int TOWER_DIAPERBOMBER_COST = 150;
	public static final int TOWER_DIAPERBOMBER_MAX_HEALTH = 500;
	public static final int TOWER_DIAPERBOMBER_RANGE = 200;
	public static final double TOWER_DIAPERBOMBER_ATTACK_INTERVAL = 2.0;
	public static final double TOWER_DIAPERBOMBER_SCALE = 1.0;
	public static final double TOWER_DIAPERBOMBER_ROTATION_SPEED = 0;

	// IED Tower vars
	public static final String TOWER_IED_NAME = "Improvised Explosive Diaper";
	public static final String TOWER_IED_DESCRIPTION = "It explodes on Dino contact.";
	public static final String TOWER_IED_IMAGE_LOCATION = "IED.png";
	public static final int TOWER_IED_IMAGE_COLS = 1;
	public static final int TOWER_IED_ATTACK_POWER = 500;
	public static final int TOWER_IED_COST = 200;
	public static final int TOWER_IED_MAX_HEALTH = 1500;
	public static final int TOWER_IED_RANGE = 75;
	public static final double TOWER_IED_ATTACK_INTERVAL = 5.0;
	public static final double TOWER_IED_SCALE = 1.0;
	public static final double TOWER_IED_ROTATION_SPEED = 0;

	// IndestructaBaby Tower Vars
	public static final String TOWER_INDESTRUCTABABY_NAME = "IndestrucataBaby!";
	public static final String TOWER_INDESTRUCTABABY_DESCRIPTION = "It can take a punch!";
	public static final String TOWER_INDESTRUCTABABY_IMAGE_LOCATION = "indestructababysprite.png";
	public static final int TOWER_INDESTRUCTABABY_IMAGE_COLS = 1;
	public static final int TOWER_INDESTRUCTABABY_ATTACK_POWER = 50;
	public static final int TOWER_INDESTRUCTABABY_COST = 300;
	public static final int TOWER_INDESTRUCTABABY_MAX_HEALTH = 2000;
	public static final int TOWER_INDESTRUCTABABY_RANGE = 200;
	public static final double TOWER_INDESTRUCTABABY_ATTACK_INTERVAL = 1.0;
	public static final double TOWER_INDESTRUCTABABY_SCALE = 1.0;
	public static final double TOWER_INDESTRUCTABABY_ROTATION_SPEED = 4;

	// InvisiBaby Tower Vars
	public static final String TOWER_INVISIBABY_NAME = "InvisiBaby!";
	public static final String TOWER_INVISIBABY_DESCRIPTION = "It can hide from Dinos!";
	public static final String TOWER_INVISIBABY_IMAGE_LOCATION = "invisibabysprite.png";
	public static final int TOWER_INVISIBABY_IMAGE_COLS = 2;
	public static final int TOWER_INVISIBABY_ATTACK_POWER = 50;
	public static final int TOWER_INVISIBABY_COST = 1500;
	public static final int TOWER_INVISIBABY_MAX_HEALTH = 500;
	public static final int TOWER_INVISIBABY_RANGE = 200;
	public static final double TOWER_INVISIBABY_ATTACK_INTERVAL = 1.0;
	public static final double TOWER_INVISIBABY_SCALE = 1.0;
	public static final double TOWER_INVISIBABY_ROTATION_SPEED = 6;

	// MachineGunBaby Tower Vars
	public static final String TOWER_MACHINEGUNBABY_NAME = "Machine Gun Baby!";
	public static final int TOWER_MACHINEGUNBABY_IMAGE_COLS = 2;
	public static final String TOWER_MACHINEGUNBABY_DESCRIPTION = "It's like a baby Rambo!";
	public static final String TOWER_MACHINEGUNBABY_IMAGE_LOCATION = "machinegunbabysprite.png";
	public static final int TOWER_MACHINEGUNBABY_ATTACK_POWER = 15;
	public static final int TOWER_MACHINEGUNBABY_COST = 800;
	public static final int TOWER_MACHINEGUNBABY_MAX_HEALTH = 500;
	public static final int TOWER_MACHINEGUNBABY_RANGE = 500;
	public static final double TOWER_MACHINEGUNBABY_ATTACK_INTERVAL = .1;
	public static final double TOWER_MACHINEGUNBABY_ROTATION_SPEED = 10;
	public static final double TOWER_MACHINEGUNBABY_SCALE = 1.5;

	// PickyEater Tower Vars
	public static final String TOWER_PICKYEATER_NAME = "Picky Eater";
	public static final String TOWER_PICKYEATER_DESCRIPTION = "Pelts Dinos with veggies.";
	public static final String TOWER_PICKYEATER_IMAGE_LOCATION = "pickyeatersprite.png";
	public static final int TOWER_PICKYEATER_IMAGE_COLS = 2;
	public static final int TOWER_PICKYEATER_ATTACK_POWER = 25;
	public static final int TOWER_PICKYEATER_COST = 300;
	public static final int TOWER_PICKYEATER_MAX_HEALTH = 400;
	public static final int TOWER_PICKYEATER_RANGE = 700;
	public static final double TOWER_PICKYEATER_ATTACK_INTERVAL = .5;
	public static final double TOWER_PICKYEATER_SCALE = 1.0;
	public static final double TOWER_PICKYEATER_ROTATION_SPEED = 8;

	// RocketBaby Tower Vars
	public static final String TOWER_ROCKETBABY_NAME = "Rocket Baby";
	public static final String TOWER_ROCKETBABY_DESCRIPTION = "It fires rockets!";
	public static final String TOWER_ROCKETBABY_IMAGE_LOCATION = "rocketbabysprite.png";
	public static final int TOWER_ROCKETBABY_IMAGE_COLS = 1;
	public static final int TOWER_ROCKETBABY_ATTACK_POWER = 150;
	public static final int TOWER_ROCKETBABY_COST = 800;
	public static final int TOWER_ROCKETBABY_MAX_HEALTH = 500;
	public static final int TOWER_ROCKETBABY_RANGE = 250;
	public static final double TOWER_ROCKETBABY_ATTACK_INTERVAL = 1;
	public static final double TOWER_ROCKETBABY_SCALE = 1.0;
	public static final double TOWER_ROCKETBABY_ROTATION_SPEED = 6;

	// SleepingBaby Tower Vars
	public static final String TOWER_SLEEPINGBABY_NAME = "Sleeping Baby";
	public static final String TOWER_SLEEPINGBABY_DESCRIPTION = "It naps. And it cries. Loud.";
	public static final String TOWER_SLEEPINGBABY_IMAGE_LOCATION = "sleepingbaby.png";
	public static final int TOWER_SLEEPINGBABY_IMAGE_COLS = 1;
	public static final int TOWER_SLEEPINGBABY_ATTACK_POWER = 10;
	public static final int TOWER_SLEEPINGBABY_COST = 300;
	public static final int TOWER_SLEEPINGBABY_MAX_HEALTH = 500;
	public static final int TOWER_SLEEPINGBABY_RANGE = 150;
	public static final double TOWER_SLEEPINGBABY_ATTACK_INTERVAL = .1;
	public static final double TOWER_SLEEPINGBABY_SCALE = 1.0;
	public static final double TOWER_SLEEPINGBABY_ROTATION_SPEED = 0;

	/* --------------------- */
	/* ---Enemy Variables--- */
	/* --------------------- */

	// TRex Vars
	public static final String ENEMY_TREX_NAME = "T-Rex";
	public static final String ENEMY_TREX_IMAGE_LOCATION = "TRexTopDownWalkingSprite2.png";
	public static final int ENEMY_TREX_IMAGE_COLS = 2;
	public static final int ENEMY_TREX_ATTACK_POWER = 100;
	public static int ENEMY_TREX_MAX_HEALTH = 400;
	public static final int ENEMY_TREX_RANGE = 20;
	public static final double ENEMY_TREX_SCALE = 1.5;
	public static final double ENEMY_TREX_SPRITE_INTERVAL = 0.2;
	public static final double ENEMY_TREX_ATTACK_INTERVAL = 1.5;
	public static final double ENEMY_TREX_ROTATION_SPEED = 5;
	public static double ENEMY_TREX_SPEED = 50;

	// Pterodactyl Vars
	public static final String ENEMY_PTERODACTYL_NAME = "Pterodactyl";
	public static final String ENEMY_PTERODACTYL_IMAGE_LOCATION = "PterFlying2.png";
	public static final int ENEMY_PTERODACTYL_IMAGE_COLS = 4;
	public static final int ENEMY_PTERODACTYL_ATTACK_POWER = 75;
	public static final int ENEMY_PTERODACTYL_MAX_HEALTH = 200;
	public static final int ENEMY_PTERODACTYL_RANGE = 30;
	public static final double ENEMY_PTERODACTYL_SCALE = 1;
	public static final double ENEMY_PTERODACTYL_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_PTERODACTYL_ATTACK_INTERVAL = 1.0;
	public static final double ENEMY_PTERODACTYL_ROTATION_SPEED = 6;
	public static final double ENEMY_PTERODACTYL_SPEED = 200;

	// Velociraptor Vars
	public static final String ENEMY_VELOCIRAPTOR_NAME = "Velociraptor";
	public static final String ENEMY_VELOCIRAPTOR_IMAGE_LOCATION = "RaptorWalkSprite.png";
	public static final int ENEMY_VELOCIRAPTOR_IMAGE_ROWS = 1;
	public static final int ENEMY_VELOCIRAPTOR_IMAGE_COLS = 2;
	public static final int ENEMY_VELOCIRAPTOR_ATTACK_POWER = 25;
	public static int ENEMY_VELOCIRAPTOR_MAX_HEALTH = 250;
	public static final int ENEMY_VELOCIRAPTOR_RANGE = 20;
	public static final double ENEMY_VELOCIRAPTOR_SCALE = 2;
	public static final double ENEMY_VELOCIRAPTOR_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_VELOCIRAPTOR_ATTACK_INTERVAL = 0.5;
	public static final double ENEMY_VELOCIRAPTOR_ROTATION_SPEED = 3;
	public static double ENEMY_VELOCIRAPTOR_SPEED = 100;

	// IEDCeratops Vars
	public static final String ENEMY_IEDCERATOPS_NAME = "IED-ceratops";
	public static final String ENEMY_IEDCERATOPS_IMAGE_LOCATION = "IEDTriSprite.png";
	public static final int ENEMY_IEDCERATOPS_IMAGE_ROWS = 1;
	public static final int ENEMY_IEDCERATOPS_IMAGE_COLS = 2;
	public static final int ENEMY_IEDCERATOPS_ATTACK_POWER = 300;
	public static int ENEMY_IEDCERATOPS_MAX_HEALTH = 600;
	public static final int ENEMY_IEDCERATOPS_RANGE = 30;
	public static final double ENEMY_IEDCERATOPS_SCALE = 2;
	public static final double ENEMY_IEDCERATOPS_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_IEDCERATOPS_ATTACK_INTERVAL = 5;
	public static final double ENEMY_IEDCERATOPS_ROTATION_SPEED = 3;
	public static double ENEMY_IEDCERATOPS_SPEED = 50;

	// TRocket Vars
	public static final String ENEMY_TROCKET_NAME = "T-Rocket";
	public static final String ENEMY_TROCKET_IMAGE_LOCATION = "TRexRocketSprite.png";
	public static final int ENEMY_TROCKET_IMAGE_ROWS = 1;
	public static final int ENEMY_TROCKET_IMAGE_COLS = 2;
	public static final int ENEMY_TROCKET_ATTACK_POWER = 150;
	public static int ENEMY_TROCKET_MAX_HEALTH = 400;
	public static final int ENEMY_TROCKET_RANGE = 200;
	public static final double ENEMY_TROCKET_SCALE = 2;
	public static final double ENEMY_TROCKET_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_TROCKET_ATTACK_INTERVAL = 1.5;
	public static final double ENEMY_TROCKET_ROTATION_SPEED = 3;
	public static double ENEMY_TROCKET_SPEED = 100;

	// Triceratops Vars
	public static final String ENEMY_TRICERITOPS_NAME = "Triceritops";
	public static final String ENEMY_TRICERITOPS_IMAGE_LOCATION = "TriWalkingSprite.png";
	public static final int ENEMY_TRICERITOPS_IMAGE_ROWS = 1;
	public static final int ENEMY_TRICERITOPS_IMAGE_COLS = 2;
	public static final int ENEMY_TRICERITOPS_ATTACK_POWER = 80;
	public static int ENEMY_TRICERITOPS_MAX_HEALTH = 600;
	public static final int ENEMY_TRICERITOPS_RANGE = 30;
	public static final double ENEMY_TRICERITOPS_SCALE = .9;
	public static final double ENEMY_TRICERITOPS_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_TRICERITOPS_ATTACK_INTERVAL = 1.5;
	public static final double ENEMY_TRICERITOPS_ROTATION_SPEED = 1.5;
	public static double ENEMY_TRICERITOPS_SPEED = 75;

	// Stealthadactyl Vars
	public static final String ENEMY_STEALTHADACTYL_NAME = "Stealthadactyl";
	public static final String ENEMY_STEALTHADACTYL_IMAGE_LOCATION = "StealthFlyingSprite.png";
	public static final int ENEMY_STEALTHADACTYL_IMAGE_ROWS = 1;
	public static final int ENEMY_STEALTHADACTYL_IMAGE_COLS = 4;
	public static final int ENEMY_STEALTHADACTYL_ATTACK_POWER = 75;
	public static final int ENEMY_STEALTHADACTYL_MAX_HEALTH = 250;
	public static final int ENEMY_STEALTHADACTYL_RANGE = 30;
	public static final double ENEMY_STEALTHADACTYL_SCALE = 0.5;
	public static final double ENEMY_STEALTHADACTYL_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_STEALTHADACTYL_ATTACK_INTERVAL = 1.0;
	public static final double ENEMY_STEALTHADACTYL_ROTATION_SPEED = 6;
	public static final double ENEMY_STEALTHADACTYL_SPEED = 200;

	// Stegasaurus Vars
	public static final String ENEMY_STEGOSAURUS_NAME = "Stegosaurus";
	public static final String ENEMY_STEGOSAURUS_IMAGE_LOCATION = "stegoWalkSprite.png";
	public static final int ENEMY_STEGOSAURUS_IMAGE_ROWS = 1;
	public static final int ENEMY_STEGOSAURUS_IMAGE_COLS = 2;
	public static final int ENEMY_STEGOSAURUS_ATTACK_POWER = 50;
	public static int ENEMY_STEGOSAURUS_MAX_HEALTH = 750;
	public static final int ENEMY_STEGOSAURUS_RANGE = 20;
	public static final double ENEMY_STEGOSAURUS_SCALE = 2;
	public static final double ENEMY_STEGOSAURUS_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_STEGOSAURUS_ATTACK_INTERVAL = 1.5;
	public static final double ENEMY_STEGOSAURUS_ROTATION_SPEED = 3;
	public static double ENEMY_STEGOSAURUS_SPEED = 50;

	// Machinegun Raptor Vars
	public static final String ENEMY_MACHINEGUNRAPTOR_NAME = "Machine Gun Raptor";
	public static final String ENEMY_MACHINEGUNRAPTOR_IMAGE_LOCATION = "MachineGunRaptorSprite.png";
	public static final int ENEMY_MACHINEGUNRAPTOR_IMAGE_ROWS = 1;
	public static final int ENEMY_MACHINEGUNRAPTOR_IMAGE_COLS = 2;
	public static final int ENEMY_MACHINEGUNRAPTOR_ATTACK_POWER = 25;
	public static int ENEMY_MACHINEGUNRAPTOR_MAX_HEALTH = 200;
	public static final int ENEMY_MACHINEGUNRAPTOR_RANGE = 100;
	public static final double ENEMY_MACHINEGUNRAPTOR_SCALE = 2;
	public static final double ENEMY_MACHINEGUNRAPTOR_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_MACHINEGUNRAPTOR_ATTACK_INTERVAL = 0.75;
	public static final double ENEMY_MACHINEGUNRAPTOR_ROTATION_SPEED = 3;
	public static double ENEMY_MACHINEGUNRAPTOR_SPEED = 100;

	// Tankosaurus Vars
	public static final String ENEMY_TANKOSAURUS_NAME = "Tank-o-saurus";
	public static final String ENEMY_TANKOSAURUS_IMAGE_LOCATION = "tankosaursprite.png";
	public static final int ENEMY_TANKOSAURUS_IMAGE_ROWS = 1;
	public static final int ENEMY_TANKOSAURUS_IMAGE_COLS = 1;
	public static final int ENEMY_TANKOSAURUS_ATTACK_POWER = 30;
	public static int ENEMY_TANKOSAURUS_MAX_HEALTH = 2000;
	public static final int ENEMY_TANKOSAURUS_RANGE = 30;
	public static final double ENEMY_TANKOSAURUS_SCALE = .9;
	public static final double ENEMY_TANKOSAURUS_SPRITE_INTERVAL = 0.1;
	public static final double ENEMY_TANKOSAURUS_ATTACK_INTERVAL = 1.5;
	public static final double ENEMY_TANKOSAURUS_ROTATION_SPEED = 3;
	public static double ENEMY_TANKOSAURUS_SPEED = 50;

	/* -------------------------- */
	/* ---Projectile Variables--- */
	/* -------------------------- */

	// Bullet Vars
	public static final String PROJECTILE_BULLETSHOT_NAME = "Bullet Shot";
	public static final String PROJECTILE_BULLETSHOT_IMAGE_LOCATION = "BulletShot.png";
	public static final double PROJECTILE_BULLETSHOT_SCALE = 1;
	public static final double PROJECTILE_ROTATION_SPEED = 0.5;
	public static final double PROJECTILE_SPEED = 600;

	// Rocket vars
	public static final String PROJECTILE_ROCKETSHOT_NAME = "Rocket Shot";
	public static final String PROJECTILE_ROCKETSHOT_IMAGE_LOCATION = "rocketshot.png";
	public static final double PROJECTILE_ROCKETSHOT_SCALE = 1.5;

	// Block vars
	public static final String PROJECTILE_BLOCKSHOT_NAME = "Block Shot";
	public static final String PROJECTILE_BLOCKSHOT_IMAGE_LOCATION = "BlockShotSprite.png";
	public static final double PROJECTILE_BLOCKSHOT_SCALE = .5;

	// Diaper vars
	public static final String PROJECTILE_DIAPERBOMB_NAME = "Diaper Bomb";
	public static final String PROJECTILE_DIAPERBOMB_IMAGE_LOCATION = "DiaperBombTrans.png";
	public static final double PROJECTILE_DIAPERBOMB_SCALE = 1;

	// Food vars
	public static final String PROJECTILE_FOODSHOT_NAME = "Food Shot";
	public static final String PROJECTILE_FOODSHOT_IMAGE_LOCATION = "FoodShotTrans.png";
	public static final double PROJECTILE_FOODSHOT_SCALE = .5;

	private GameVars() {
	}

}
