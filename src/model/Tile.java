package model;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import model.Boards.BoardType;
import model.gameObject.GameObject;

public class Tile implements Drawable {

	private int row, col;
	private GameObject object;
	private Rectangle frame;
	private Board board;
	private TileType type;
	private BufferedImage background;
	private BufferedImage layer;

	public Tile(int row, int col, Rectangle frame, Board board) {
		this.row = row;
		this.col = col;
		this.frame = frame;
		this.board = board;
		setBoardType(board.getBoardType());
		setTileType(TileType.both);
	}

	public Tile(int row, int col, Rectangle frame, Board board, TileType type) {
		this(row, col, frame, board);
		setTileType(type);
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public void setTileType(TileType type) {
		this.type = type;
		if (type == TileType.enemyOnly)
			layer = GameToolbox.getImage(board.getBoardType()
					.getEnemyOnlyImage());
		else if (type == TileType.towerOnly)
			layer = GameToolbox.getImage(board.getBoardType()
					.getTowerOnlyImage());
		else if (type == TileType.none)
			layer = GameToolbox.getImage(board.getBoardType().getNoneImage());
		else
			layer = null;
	}

	public void setBoardType(BoardType bt) {
		background = GameToolbox.getImage(bt.getBackgroundFilename());
	}

	public TileType getTileType() {
		return type;
	}

	public boolean contains(double x, double y) {
		return frame.contains(x, y);
	}

	public boolean addObject(GameObject object) {
		if (!canBuild())
			return false;
		this.object = object;
		object.moveTo(frame.getCenterX(), frame.getCenterY());
		return true;
	}

	public Rectangle getFrame() {
		return new Rectangle(frame);
	}

	public void removeObject() {
		this.object = null;
		board.removeObject();
	}

	public GameObject getGameObject() {
		return object;
	}

	public boolean hasObject() {
		return object != null;
	}

	public void addLayer(BufferedImage layer) {
		this.layer = layer;
	}

	public boolean canPass() {
		if (hasObject())
			return false;
		return type.canPass();
	}

	public boolean canBuild() {
		if (hasObject())
			return false;
		return type.canBuild();
	}

	public int getX() {
		return (int) frame.getCenterX();
	}

	public int getY() {
		return (int) frame.getCenterY();
	}

	public String toString() {
		return "{Row=" + row + ", Col=" + col + ", Type=" + type + "}";
	}

	@Override
	public void draw(Graphics2D g) {
		if (g != null) {
			g.drawImage(background, GameToolbox.roundDoubleToInt(frame.getX()),
					GameToolbox.roundDoubleToInt(frame.getY()),
					GameToolbox.roundDoubleToInt(frame.getWidth()),
					GameToolbox.roundDoubleToInt(frame.getHeight()), null);
			if (layer != null) {
				g.drawImage(layer, GameToolbox.roundDoubleToInt(frame.getX()),
						GameToolbox.roundDoubleToInt(frame.getY()),
						GameToolbox.roundDoubleToInt(frame.getWidth()),
						GameToolbox.roundDoubleToInt(frame.getHeight()), null);
			}
		}
		// g.draw(frame);
	}
}
