package model;

import gui.GameMenu;
import gui.InfoBar;
import gui.MainMenu;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;

import model.Boards.BoardType;
import model.Towers.BlockBombarder;
import model.Towers.DiaperBomber;
import model.Towers.IED;
import model.Towers.IndestructaBaby;
import model.Towers.InvisiBaby;
import model.Towers.MachineGunBaby;
import model.Towers.PickyEater;
import model.Towers.RocketBaby;
import model.Towers.SleepingBaby;
import model.Towers.Tower;
import model.gameObject.GameObject;
import model.gameObject.Nexus;
import server.BoardInfo;
import server.GameCommand;
import server.GameExecutable;
import server.GamePacket;
import server.GameServer;

@SuppressWarnings("serial")
public class Game extends Thread implements GameExecutable, Serializable {

	private long time = System.currentTimeMillis();
	private int frames = 0;
	private String currentFPS = "";
	private boolean showFPS = true;

	private int height, width;
	private boolean paused = true;
	private int timeScalePreset = GameVars.TIMESCALE_DEFAULT;
	private double timeScale = GameVars.TIMESCALE_PRESETS[timeScalePreset];

	private int mode;

	private Map map;
	private User user;

	private GameMenu rootMenu;

	private Deque<GameCommand> commandQueue;
	private boolean multiplayer = false;

	private GameServer server;

	private Board board;
	private Spawner spawner;
	private Nexus nexus;
	private boolean gameOver;

	private String clientName = "";

	private Tower towerToPlace;

	public Game(int width, int height) {
		this.height = height;
		this.width = width;
		GameToolbox.cachConstants();
		initializeMenuObjects();
		setMode(GameVars.GAMEMODE_MAINMENU);
		commandQueue = new ArrayDeque<GameCommand>();
		start();
	}

	public Game(int width, int height, GameServer server) {
		this.height = height;
		this.width = width;
		GameToolbox.cachConstants();
		this.server = server;
		initializeMenuObjects();
		setMode(GameVars.GAMEMODE_MAINMENU);
		commandQueue = new ArrayDeque<GameCommand>();
		start();

	}

	// public Game(int width, int height, String name, int id) {
	// user = new User(name);
	// user.setId(id);
	// this.height = height;
	// this.width = width;
	//
	// initializeObjects();
	// start();
	// pauseGame();
	// }
	//
	// public Game() {
	//
	// this.width = GameVars.DEFAULT_FRAME_WIDTH;
	// this.height = GameVars.DEFAULT_FRAME_HEIGHT;
	// initializeObjects();
	// start();
	// }

	public String getLevel() {
		if (multiplayer)
			return "";
		return String.valueOf(spawner.getLevel());
	}

	public void setSize(int width, int height) {
		this.height = height;
		this.width = width;
		map.setSize(width, height);
	}

	private void initializeMenuObjects() {
		rootMenu = new MainMenu(this);
		if (server != null)
			map = new Map(width, height, server);
		else
			map = new Map(width, height);

	}

	private void initializeGameObjects() {
		rootMenu = new InfoBar(this);
		user = new User("Simon");
		map.giveUser(user);
		board = new Board(GameVars.BOARD_X_OFFSET, GameVars.BOARD_Y_OFFSET,
				width - GameVars.BOARD_X_OFFSET * 2, height
						- GameVars.BOARD_Y_OFFSET * 2,
				GameVars.BOARD_DEFAULT_ROWS, GameVars.BOARD_DEFAULT_COLS,
				BoardType.playground);
		if (!multiplayer) {
			nexus = new Nexus(500, 500, map);
			spawner = new Spawner(map, board);
			// towerToPlace = new MachineGunBaby(-100, -100, null);
			// towerToPlace.setOpacity(0.7f);
		}
	}

	private void initializeGameObjects(int boardType) {
		rootMenu = new InfoBar(this);
		user = new User("Simon");
		map.giveUser(user);
		BoardType type = BoardType.naproom;
		switch (boardType) {
		case 1:
			type = BoardType.playground;
			break;
		case 2:
			type = BoardType.playroom;
			break;
		case 3:
			type = BoardType.naproom;
			break;
		}
		board = new Board(GameVars.BOARD_X_OFFSET, GameVars.BOARD_Y_OFFSET,
				width - GameVars.BOARD_X_OFFSET * 2, height
						- GameVars.BOARD_Y_OFFSET * 2,
				GameVars.BOARD_DEFAULT_ROWS, GameVars.BOARD_DEFAULT_COLS, type);
		if (!multiplayer) {
			nexus = new Nexus(675, 350, map);
			spawner = new Spawner(map, board);
			// towerToPlace = new MachineGunBaby(-100, -100, null);
			// towerToPlace.setOpacity(0.7f);
		}
	}

	public void setTimeScale(double scale) {
		timeScale = scale;
	}

	public double getTimeScale() {
		return timeScale;
	}

	public void increaseTimescale() {
		if (timeScalePreset < GameVars.TIMESCALE_PRESETS.length - 1)
			setTimeScale(GameVars.TIMESCALE_PRESETS[++timeScalePreset]);
	}

	public void decreaseTimescale() {
		if (timeScalePreset > 0)
			setTimeScale(GameVars.TIMESCALE_PRESETS[--timeScalePreset]);
	}

	private void updateGame(long milliseconds) {
		double deltaT = (milliseconds / 1000.0) * timeScale;
		if (!multiplayer) {
			if (map != null)
				for (Updateable u : map.getUpdateableObjects())
					u.update(deltaT);
			spawner.update(deltaT);
		} else
			for (Updateable u : map.getUpdateableObjects())
				u.updateSprite(deltaT);
		if (!multiplayer && !nexus.isAlive())
			quitGame();
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public void pauseGame() {
		setMode(GameVars.GAMEMODE_PAUSEMENU);
		paused = true;
	}

	public void resumeGame() {
		setMode(GameVars.GAMEMODE_INGAME);
		paused = false;
	}

	public void startGame() {
		initializeGameObjects();
		resumeGame();
		// setMode(GameVars.GAMEMODE_TOWERPLACEMENT);
	}

	public void startGame(int boardType) {
		initializeGameObjects(boardType);
		resumeGame();
		// setMode(GameVars.GAMEMODE_TOWERPLACEMENT);
	}

	// public void startMultiplayer() {
	//
	// }

	public void hostMultiplayer() {
		new GameServer(9001);
		new ClientThread("localhost", 9001, this).start();
		multiplayer = true;
	}

	public void joinMultiplayer() {
		new ClientThread("localhost", 9001, this).start();
		multiplayer = true;
	}

	public void quitGame() {
		pauseGame();
		map.clearMap();
		board = null;
		spawner = null;
		multiplayer = false;
		setMode(GameVars.GAMEMODE_MAINMENU);
		rootMenu = new MainMenu(this);
		gameOver = false;
	}

	public boolean isPaused() {
		return paused;
	}

	public Drawable[] getBackground() {
		Drawable[] background = { board };
		return background;
	}

	public Drawable[] getDrawableObjects() {
		return map.getDrawableObjects();
	}

	public User getUser() {
		return user;
	}

	public Map getMap() {
		return map;
	}

	public void setShowFPS(boolean showFPS) {
		this.showFPS = showFPS;
	}

	public Board getBoard() {
		return board;
	}

	public Tower getTower(String tower) {
		tower = tower.toUpperCase();
		switch (tower) {
		case "BLOCKBOMBARDER":
			return new BlockBombarder(-100, -100, null);
		case "DIAPERBOMBER":
			return new DiaperBomber(-100, -100, null);
		case "IED":
			return new IED(-100, -100, null);
		case "INDESTRUCTABABY":
			return new IndestructaBaby(-100, -100, null);
		case "INVISIBABY":
			return new InvisiBaby(-100, -100, null);
		case "MACHINEGUNBABY":
			return new MachineGunBaby(-100, -100, null);
		case "PICKYEATER":
			return new PickyEater(-100, -100, null);
		case "ROCKETBABY":
			return new RocketBaby(-100, -100, null);
		case "SLEEPINGBABY":
			return new SleepingBaby(-100, -100, null);

		}
		return null;
	}

	public void placeTower(int row, int col, String tower) {
		Tower t = getTower(tower);
		board.addObject(row, col, t);
		t.setTileOn(board.getTile(row, col));
		t.setMap(map);
	}

	public void placeTowerMode(String tower) {
		towerToPlace = getTower(tower);
		user.setCurrency(user.getCurrency() - towerToPlace.getCost());
		towerToPlace.setOpacity(0.7f);
		setMode(GameVars.GAMEMODE_TOWERPLACEMENT);
	}

	public void draw(Graphics2D g) {
		Drawable[] db = getBackground();
		if (db[0] != null)
			for (Drawable b : db)
				b.draw(g);
		Drawable[] dO = getDrawableObjects();
		for (Drawable o : dO)
			o.draw(g);
		rootMenu.draw(g);
		if (showFPS) {
			g.setColor(Color.GREEN);
			g.drawString(currentFPS, 5, 15);
		}
		frames++;
		long timeSince = System.currentTimeMillis() - time;
		if (timeSince >= GameVars.FPS_UPDATE_INTERVAL) {
			currentFPS = ("FPS: " + GameToolbox.roundDouble(frames
					/ (timeSince / 1000.0)));
			time = System.currentTimeMillis();
			frames = 0;
		}
	}

	public void executeCommand(int command) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			return;
		case GameVars.COMMAND_START_EASY:
			startGame(1);
			break;
		case GameVars.COMMAND_START_MED:
			startGame(2);
			break;
		case GameVars.COMMAND_START_HARD:
			startGame(3);
			break;
		case GameVars.COMMAND_HOST_MULTI:
			hostMultiplayer();
			break;
		case GameVars.COMMAND_JOIN_MULTI:
			joinMultiplayer();
			break;
		case GameVars.COMMAND_OPTIONS:
			break;
		case GameVars.COMMAND_PAUSE:
			if (multiplayer)
				commandQueue.addLast(new GameCommand(GameVars.COMMAND_PAUSE,
						"TODO: add name"));
			else
				pauseGame();
			break;
		case GameVars.COMMAND_RESUME:
			if (multiplayer)
				commandQueue.addLast(new GameCommand(GameVars.COMMAND_RESUME,
						"TODO: add name"));
			else
				resumeGame();
			break;
		case GameVars.COMMAND_INCREASE_TIMESCALE:
			if (multiplayer)
				commandQueue.addLast(new GameCommand(
						GameVars.COMMAND_INCREASE_TIMESCALE, "TODO: add name"));
			else
				increaseTimescale();
			break;
		case GameVars.COMMAND_DECREASE_TIMESCALE:
			if (multiplayer)
				commandQueue.addLast(new GameCommand(
						GameVars.COMMAND_DECREASE_TIMESCALE, "TODO: add name"));
			else
				decreaseTimescale();
			break;
		case GameVars.COMMAND_QUIT_GAME:
			quitGame();
			break;
		}
	}

	@Override
	public void executeCommand(int command, String source) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			break;
		case GameVars.COMMAND_PAUSE:
			pauseGame();
			rootMenu.openNextMenu("Pause Game");
			break;
		case GameVars.COMMAND_RESUME:
			resumeGame();
			rootMenu.closeNextMenu();
			break;
		case GameVars.COMMAND_INCREASE_TIMESCALE:
			increaseTimescale();
			break;
		case GameVars.COMMAND_DECREASE_TIMESCALE:
			decreaseTimescale();
			break;
		default:
			executeCommand(command);
		}
	}

	@Override
	public void executeCommand(int command, String source, Object[] args) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			break;
		case GameVars.COMMAND_ADD_OBJECT:
			addObject(args);
			break;
		case GameVars.COMMAND_REMOVE_OBJECT:
			map.removeGameObject((Integer) args[0]);
			break;
		case GameVars.COMMAND_SET_BOARD:
			board.setBoardInfo((BoardInfo) args[0]);
			break;
		case GameVars.COMMAND_PLACE_TOWER:
			placeTowerMode((String) args[0]);
			break;
		default:
			executeCommand(command, source);
		}
	}

	private void addObject(Object[] args) {
		Class c = (Class) args[0];
		Constructor constructor = c.getConstructors()[0];
		Object[] params = new Object[constructor.getParameterTypes().length];
		params[0] = args[1];
		params[1] = args[2];
		params[2] = map;
		GameObject go = null;
		try {
			go = (GameObject) constructor.newInstance(params);
			go.setID((Integer) args[3]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// if (go != null && go instanceof Tower)
		// {
		// board.addObject((int) args[4], (int) args[5], go);
		// ((Tower)go).setTileOn(board.getTile((int)args[4], (int) args[5]));
		// }
		// new TRex((Integer) args[1], (Integer) args[2], map, new
		// LinearPath(0))
		// .setID((Integer) args[3]);
	}

	public GamePacket getPacket() {
		GamePacket packet = new GamePacket(map.getObjectPackets());
		return packet;
	}

	public void update(GamePacket packet) {
		map.update(packet.getObjectPackets());
	}

	public void setMode(int mode) {
		this.mode = mode;
		switch (mode) {
		case GameVars.GAMEMODE_MAINMENU:
			break;
		case GameVars.GAMEMODE_INGAME:
			break;
		case GameVars.GAMEMODE_PAUSEMENU:
			break;
		}
	}

	public int getMode() {
		return mode;
	}

	public void mousePressed(int x, int y) {

		rootMenu.mousePressed(x, y);
	}

	public void mouseMoved(int x, int y) {
		rootMenu.mouseMoved(x, y);
		if (mode == GameVars.GAMEMODE_TOWERPLACEMENT)
			board.drawAvailible(x, y, towerToPlace);
	}

	public void mouseReleased(int x, int y) {
		rootMenu.mouseReleased(x, y);
	}

	public void mouseClicked(int x, int y) {
		if (mode == GameVars.GAMEMODE_TOWERPLACEMENT) {
			if (!board.hasAvailable()) {
				board.drawAvailible(x, y, towerToPlace);
				return;
			}
			Tile t = board.getTileFromPoint(x, y);
			if (t.canBuild()
					&& board.addObject(t.getRow(), t.getCol(), towerToPlace)) {
				if (multiplayer) {
					Object[] args = { t.getRow(), t.getCol(),
							towerToPlace.getClass().getSimpleName(), };
					commandQueue.addLast(new GameCommand(
							GameVars.COMMAND_PLACE_TOWER, "", args));
					t.removeObject();
				} else {
					towerToPlace.setOpacity(1f);
					towerToPlace.setMap(map);
					towerToPlace.setTileOn(t);
				}
				board.clearAvailable();
				towerToPlace = null;
				setMode(GameVars.GAMEMODE_INGAME);
			}
		}
	}

	@Override
	public void run() {
		while (true) {
			long lastTime = System.currentTimeMillis()
					- GameVars.TARGET_UPDATE_PERIOD;
			if (!paused) {
				long currentTime = System.currentTimeMillis();
				long deltaTime = currentTime - lastTime;
				lastTime = System.currentTimeMillis();
				updateGame(deltaTime);
				deltaTime = System.currentTimeMillis() - currentTime;
				if (deltaTime > GameVars.TARGET_UPDATE_PERIOD)
					System.out.println("Update took "
							+ (deltaTime - GameVars.TARGET_UPDATE_PERIOD)
							+ "ms longer than the target.");
				if (GameVars.TARGET_UPDATE_PERIOD - deltaTime > 0)
					try {
						sleep(GameVars.TARGET_UPDATE_PERIOD - deltaTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			} else {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				lastTime = System.currentTimeMillis();
			}

		}
	}

	private class ClientThread extends Thread {
		private Game game;
		private ObjectInputStream input;
		private ObjectOutputStream output;

		public ClientThread(String address, int port, Game game) {
			this.game = game;
			try {
				Socket socket = new Socket(address, port);
				input = new ObjectInputStream(socket.getInputStream());
				output = new ObjectOutputStream(socket.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			while (true) {
				try {
					Object ob = input.readObject();
					if (ob instanceof GamePacket)
						update((GamePacket) ob);
					else
						((GameCommand) ob).execute(game);
					while (!commandQueue.isEmpty()) {
						output.writeObject(commandQueue.pollFirst());
					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

}