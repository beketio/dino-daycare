package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;

public class Stealthadactyl extends Enemy {

	public Stealthadactyl (int x, int y, Map m, GamePath p) {
		super(x,y,m,p);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 20);
		setMaxHealth(maxHealth + 80);

	}

	@Override
	public void attack(Tower target) {
		//add attack animation?
		target.takeDamage(this.getAttackPower());
	}

}


//package model.Enemies;
//
//import model.GameToolbox;
//import model.Map;
//import model.Towers.Tower;
//import model.gameObject.GamePath;
//
//public class Stealthadactyl extends Enemy {
//
//	double stealthTimer = 0;
//	double stealthInterval = 1.0;
//
//	public Stealthadactyl(int x, int y, Map m, GamePath p) {
//		super(x, y, m, p);
//		level = 1;
//	}
//
//	@Override
//	public void levelUp() {
//		level += 1;
//		setAttackpower(attackPower + 20);
//		setMaxHealth(maxHealth + 80);
//
//	}
//
//	@Override
//	public void update(double seconds) {
//		super.update(seconds);
//		if (stealthTimer >= stealthInterval && !isInvis) {
//			setSprites(GameToolbox.getImagesFromSpritesheet(
//					"StealthadactylInvisSprite.png", 1, 2));
//			isInvis = true;
//			stealthTimer = 0;
//		} else if (stealthTimer >= stealthInterval && isInvis) {
//			setSprites(GameToolbox.getImagesFromSpritesheet(
//					"StealthFlyingSprite.png", 1, 2));
//			isInvis = false;
//			stealthTimer = 0;
//		}
//
//		stealthTimer += seconds;
//	}
//
//	@Override
//	public void attack(Tower target) {
//		// TODO Auto-generated method stub
//		target.takeDamage(this.getAttackPower());
//
//	}
//
//}
