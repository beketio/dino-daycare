package model.Enemies;

import model.GameToolbox;
import model.Levelable;
import model.Map;
import model.Towers.InvisiBaby;
import model.Towers.Tower;
import model.gameObject.GameObject;
import model.gameObject.GamePath;
import model.gameObject.MoveableGameObject;
import model.gameObject.Nexus;

public abstract class Enemy extends MoveableGameObject implements Levelable {
	protected int level;
	protected double attackTimer = 0;

	public Enemy(int x, int y, Map m, GamePath p) {
		super(x, y, m, p);
	}

	@Override
	public void update(double seconds) {
		// IMPORTANT!!!
		// REMEMBER TO ADD super.update(seconds) OR ELSE IT WILL NOT WORK!!!
		super.update(seconds);
		for (GameObject curr : map.getGameObjects()) {
			if (curr instanceof Tower && !(curr instanceof InvisiBaby)) {
				if (GameToolbox.getDistance(this.getXpos(), this.getYpos(),
						curr.getXpos(), curr.getYpos()) < range) {
					if (attackTimer <= 0) {
						curr.takeDamage(this.getAttackPower());
						attackTimer = attackInterval;
					}
				}
			} else if (curr instanceof Nexus) {
				if (this.intersects(curr)) {
					this.setSpeed(0);
					if (attackTimer <= 0) {
						curr.takeDamage(this.getAttackPower());
						attackTimer = attackInterval;
					}
				}
			}
		}
		attackTimer -= seconds;
	}

	public abstract void attack(Tower target);

}
