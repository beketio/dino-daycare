package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;
import model.visual.Explosion;

public class IEDceratops extends Enemy {

	public IEDceratops(int x, int y, Map m, GamePath p) {
		super(x, y, m, p);
		level = 1;
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 20);
		setMaxHealth(maxHealth + 80);

	}

	@Override
	public void attack(Tower target) {
		target.takeDamage(this.getAttackPower());
		new Explosion(GameToolbox.roundDoubleToInt(target.getXpos()),
				GameToolbox.roundDoubleToInt(target.getYpos()), this.map);

	}

	@Override
	public void removeFromMap() {
		super.removeFromMap();
		// TODO implement this for all dinos with death animations
	}

}
