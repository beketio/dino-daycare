package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;

public class Velociraptor extends Enemy {

	public Velociraptor(int x, int y, Map m, GamePath p) {
		super(x,y,m,p);
		level = 1;
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 10);
		setMaxHealth(maxHealth + 50);
	}


	@Override
	public void attack(Tower target) {
		//TODO add attacking animation command
		target.takeDamage(attackPower);

	}

}
