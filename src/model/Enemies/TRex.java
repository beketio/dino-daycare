package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;

public class TRex extends Enemy {

	public TRex(int x, int y, Map m, GamePath p) {
		super(x, y, m, p);
		level = 1;
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 20);
		setMaxHealth(maxHealth + 80);
	}

	@Override
	public void attack(Tower target) {
		// TODO add attack animation?
		target.takeDamage(this.getAttackPower());
	}

	@Override
	public void removeFromMap() {
		super.removeFromMap();

	}

}
