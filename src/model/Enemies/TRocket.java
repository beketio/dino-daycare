package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;
import model.projectiles.RocketShot;

public class TRocket extends Enemy {

	public TRocket(int x, int y, Map m, GamePath p) {
		super(x, y, m, p);
		level = 1;
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 20);
		setMaxHealth(maxHealth + 80);

	}

	@Override
	public void attack(Tower target) {
		new RocketShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), map, target, this);

	}

}
