package model.Enemies;

import model.GameToolbox;
import model.Map;
import model.Towers.Tower;
import model.gameObject.GamePath;

public class Pterodactyl extends Enemy {

	public Pterodactyl(int x, int y, Map m, GamePath p) {
		super(x,y,m,p);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 20);
		setMaxHealth(maxHealth + 80);

	}

	@Override
	public void attack(Tower target) {
		//add attack animation?
		target.takeDamage(this.getAttackPower());
	}
	
	

}
