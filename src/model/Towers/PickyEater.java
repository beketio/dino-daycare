package model.Towers;

import java.awt.Point;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.projectiles.FoodShot;

public class PickyEater extends Tower {

	public PickyEater(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void attack(Enemy e) {
		new FoodShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);

	}

}
