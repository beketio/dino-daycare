package model.Towers;

import java.awt.Point;

import model.GameToolbox;
import model.GameVars;
import model.Map;
import model.Enemies.Enemy;
import model.gameObject.GameObject;

public class SleepingBaby extends Tower {

	public SleepingBaby(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void update(double seconds) {
		boolean didAttack = false;
		boolean enemyInRange = false;
		for(GameObject curr : this.map.getGameObjects()){
			if(curr instanceof Enemy){
				if(GameToolbox.getDistance(this.getXpos(), this.getYpos(), curr.getXpos(), curr.getYpos()) < range){
					enemyInRange = true;
					if(attackTimer <= 0){
						attack((Enemy) curr);
						didAttack = true;
					}
				}
				
			}
		}
		if(didAttack) attackTimer = attackInterval;
		attackTimer -= seconds;
		
		if(enemyInRange){
			this.setImage(GameToolbox.getImage("cryingbabysprite.png"));
		}
		else this.setImage(GameToolbox.getImage("sleepingbabysprite.png"));
		
	}

	@Override
	public void attack(Enemy e) {
		e.takeDamage(this.getAttackPower());
		
	}

}
