package model.Towers;

import model.GameToolbox;
import model.Levelable;
import model.Map;
import model.Tile;
import model.Enemies.Enemy;
import model.Enemies.Stealthadactyl;
import model.gameObject.GameObject;

public abstract class Tower extends GameObject implements Levelable {

	protected int level;
	protected double attackTimer = 0;
	private Tile tileOn;

	public Tower(int x, int y, Map m) {
		super(x, y, m);
		level = 1;
	}

	public void setTileOn(Tile tileOn) {
		this.tileOn = tileOn;
	}

	public Tile getTileOn() {
		return tileOn;
	}

	protected void bypassUpdate(double seconds) {
		super.update(seconds);
	}

	@Override
	public void removeFromMap() {
		tileOn.removeObject();
		super.removeFromMap();
	}

	public void update(double seconds) {
		super.update(seconds);
		for (GameObject curr : map.getGameObjects())
			if (curr instanceof Enemy) {
				if (!(curr instanceof Stealthadactyl)) {
					double distance = GameToolbox.getDistance(curr.getXpos(),
							curr.getYpos(), this.getXpos(), this.getYpos());
					if (distance <= range) {
						setRotation(GameToolbox.getAngle(getXpos(), getYpos(),
								curr));
						if (attackTimer <= 0 && distance <= range) {
							attack((Enemy) curr);
							attackTimer = attackInterval;
						}
						break;
					}
				} else {
					double distance = GameToolbox.getDistance(curr.getXpos(),
							curr.getYpos(), this.getXpos(), this.getYpos());
					if (distance <= range + 50) {
						setRotation(GameToolbox.getAngle(getXpos(), getYpos(),
								curr));
						if (attackTimer <= 0 && distance <= range
								&& !curr.isInvis) {
							attack((Enemy) curr);
							attackTimer = attackInterval;
						}
						break;
					}
				}
			}
		attackTimer -= seconds;
	}

	public abstract void attack(Enemy e);

}
