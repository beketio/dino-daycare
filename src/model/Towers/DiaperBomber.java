package model.Towers;

import java.awt.Point;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.projectiles.BlockShot;
import model.projectiles.DiaperBomb;

public class DiaperBomber extends Tower {

	public DiaperBomber(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 35);
		setMaxHealth(maxHealth + 100);
	}

	@Override
	public void attack(Enemy e) {
		new DiaperBomb(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);

	}

}
