package model.Towers;

import java.awt.Point;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.projectiles.BulletShot;

public class MachineGunBaby extends Tower {

	public MachineGunBaby(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void attack(Enemy e) {
		new BulletShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);

	}

}
