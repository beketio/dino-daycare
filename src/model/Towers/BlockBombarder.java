package model.Towers;

import java.awt.Point;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.paths.LinearPath;
import model.projectiles.BlockShot;

public class BlockBombarder extends Tower {

	public BlockBombarder(int x, int y, Map m) {
		super(x, y, m);
		setRotation(Math.PI);

	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void attack(Enemy e) {
		new BlockShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);
	}

}
