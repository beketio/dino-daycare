package model.Towers;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.projectiles.RocketShot;

public class RocketBaby extends Tower {

	public RocketBaby(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void attack(Enemy e) {
		new RocketShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);

	}

}
