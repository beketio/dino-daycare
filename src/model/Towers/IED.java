package model.Towers;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.Enemies.Stealthadactyl;
import model.gameObject.GameObject;
import model.visual.Explosion;

public class IED extends Tower {

	public IED(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
	}

	@Override
	public void attack(Enemy e) {
		new Explosion(GameToolbox.roundDoubleToInt(getXpos()),
				GameToolbox.roundDoubleToInt(getYpos()), map, this)
				.setScale(0.3);
		isAlive = false;
		for (GameObject curr : map.getGameObjects())
			if (curr instanceof Enemy) {
				double distance = GameToolbox.getDistance(curr.getXpos(),
						curr.getYpos(), this.getXpos(), this.getYpos());
				if (distance <= range) {
					curr.takeDamage(this.attackPower);
				}
			}
		// for (GameObject curr : map.getGameObjects()) {
		// if (curr instanceof Enemy) {
		// curr.takeDamage(this.attackPower);
		// }
		// }
		// this.removeFromMap();
	}

}
