package model.Towers;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.projectiles.BulletShot;

public class InvisiBaby extends Tower {

	public InvisiBaby(int x, int y, Map m) {
		super(x, y, m);
	}

	@Override
	public void levelUp() {
		level += 1;
		setAttackpower(attackPower + 50);
		setMaxHealth(maxHealth + 100);

	}

	@Override
	public void attack(Enemy e) {
		new BulletShot(GameToolbox.roundDoubleToInt(this.getXpos()),
				GameToolbox.roundDoubleToInt(this.getYpos()), this.map, e, this);

	}

}
