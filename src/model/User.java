package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class User implements Serializable {

	private long currency;
	private int id;
	private String name;

	public User(String name) {
		this.name = name;
		this.currency = 150000;
	}
	
	/*************************************************************/
	/****************** Getters and Setters **********************/
	/*************************************************************/
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCurrency() {
		return currency;
	}

	public void setCurrency(long currency) {
		this.currency = currency;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
