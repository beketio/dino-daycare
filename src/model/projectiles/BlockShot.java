package model.projectiles;

import model.GameToolbox;
import model.Map;
import model.Enemies.Enemy;
import model.gameObject.GameObject;
import model.gameObject.GamePath;
import model.gameObject.Projectile;

public class BlockShot extends HomingProjectile {

	public BlockShot(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, trackedObject, origin);
		setSpeed(150);
		setImage(GameToolbox.getImage("DiaperBombTrans.png"));
	}
}
