package model.projectiles;

import model.Map;
import model.gameObject.GameObject;
import model.gameObject.Projectile;
import model.paths.LinearPath;

public class LinearProjectile extends Projectile {

	public LinearProjectile(int x, int y, Map map, double scale,
			double direction, GameObject origin) {
		super(x, y, map, new LinearPath(direction), origin);
	}

}
