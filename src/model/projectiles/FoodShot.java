package model.projectiles;

import model.GameToolbox;
import model.Map;
import model.gameObject.GameObject;

public class FoodShot extends HomingProjectile {

	public FoodShot(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, trackedObject, origin);
		setSpeed(200);
		setImage(GameToolbox.getImage("FoodShotTrans.png"));
	}

}
