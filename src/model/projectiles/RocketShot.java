package model.projectiles;

import model.GameToolbox;
import model.Map;
import model.gameObject.GameObject;

public class RocketShot extends HomingProjectile {

	public RocketShot(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, trackedObject, origin);
		setSpeed(250);
		setImage(GameToolbox.getImage("RocketShot.png"));
	}

}
