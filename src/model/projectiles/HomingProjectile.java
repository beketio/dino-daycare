package model.projectiles;

import model.Map;
import model.gameObject.GameObject;
import model.gameObject.Projectile;
import model.paths.HomingPath;

public class HomingProjectile extends Projectile {

	public HomingProjectile(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, new HomingPath(trackedObject), origin);
	}
}
