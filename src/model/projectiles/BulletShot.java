package model.projectiles;

import model.Map;
import model.gameObject.GameObject;

public class BulletShot extends HomingProjectile {

	public BulletShot(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, trackedObject, origin);
	}

}
