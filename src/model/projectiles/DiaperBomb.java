package model.projectiles;

import model.GameToolbox;
import model.Map;
import model.gameObject.GameObject;

public class DiaperBomb extends HomingProjectile {

	public DiaperBomb(int x, int y, Map map, GameObject trackedObject,
			GameObject origin) {
		super(x, y, map, trackedObject, origin);
		
		setSpeed(100);
		setImage(GameToolbox.getImage("DiaperBombTrans.png"));
	}

}
