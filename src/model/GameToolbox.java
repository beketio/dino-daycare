package model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import model.gameObject.GameObject;

public class GameToolbox {

	public static int roundDoubleToInt(double num) {
		return (int) (num + 0.5);
	}

	public static double roundDouble(double num) {
		return roundDouble(num, 1);
	}

	public static double roundDouble(double num, int decimals) {
		double mult = 10.0 * decimals;
		return ((int) (num * mult)) / mult;
	}

	public static double getDistance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow((y2 - y1), 2) + Math.pow(x2 - x1, 2));
	}

	public static double getDistance(Tile t1, Tile t2) {
		return getDistance(t1.getX(), t1.getY(), t2.getX(), t2.getY());
	}

	public static double getAngle(double x1, double y1, double x2, double y2) {
		double yV = (y2 - y1);
		double xV = (x2 - x1);
		double rotation = Math.atan(xV / yV) + Math.PI / 2;
		if ((xV > 0 && yV >= 0) || (xV <= 0 && yV >= 0))
			rotation += Math.PI;
		return rotation;
	}

	public static double getAngle(double x, double y, GameObject object) {
		return getAngle(x, y, object.getXpos(), object.getYpos());
	}

	private static HashMap<String, BufferedImage> imageMap = new HashMap<String, BufferedImage>();

	public static BufferedImage getImage(String imgName) {
		if (imageMap.containsKey(imgName))
			return imageMap.get(imgName);
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(GameVars.IMAGE_DIRECTORY + imgName));
		} catch (IOException e) {
			System.out.println("Could not find " + imgName + ", using "
					+ GameVars.DEFAULT_IMAGE_LOCATION);
			try {
				img = ImageIO.read(new File(GameVars.IMAGE_DIRECTORY
						+ GameVars.DEFAULT_IMAGE_LOCATION));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		imageMap.put(imgName, img);
		return img;
	}

	private static HashMap<String, BufferedImage[]> spriteMap = new HashMap<String, BufferedImage[]>();

	private static BufferedImage[] getImagesFromSpritesheet(BufferedImage img,
			int rows, int cols) {
		int width = img.getWidth() / cols;
		int height = img.getHeight() / rows;
		BufferedImage[] imgs = new BufferedImage[rows * cols];
		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++)
				imgs[c + r * cols] = img.getSubimage(c * width, r * height,
						width, height);
		return imgs;
	}

	public static BufferedImage[] getImagesFromSpritesheet(String imgName,
			int width, int height) {
		if (spriteMap.containsKey(imgName))
			return spriteMap.get(imgName);
		BufferedImage[] sprite = getImagesFromSpritesheet(getImage(imgName),
				width, height);
		spriteMap.put(imgName, sprite);
		return sprite;
	}

	private static HashMap<String, Object> constantMap = new HashMap<String, Object>();
	private static String[] towerList = new String[0],
			enemyList = new String[0];

	public static void cachConstants() {
		try {
			Field[] fields = GameVars.class.getDeclaredFields();
			List<String> towers = new ArrayList<String>();
			List<String> enemies = new ArrayList<String>();
			for (Field field : fields) {
				String fieldName = field.getName();
				String name = parseFieldName(fieldName);
				if (fieldName.startsWith("TOWER")) {
					constantMap.put(name, field.get(null));
					if (name.contains("_NAME"))
						towers.add(name.substring(0, name.lastIndexOf('_')));
				} else if (fieldName.startsWith("ENEMY")) {
					constantMap.put(name, field.get(null));
					if (name.contains("_NAME"))
						enemies.add(name.substring(0, name.lastIndexOf('_')));
				} else if (fieldName.startsWith("DEFAULT")
						|| fieldName.startsWith("PROJECTILE"))
					constantMap.put(name, field.get(null));
			}
			towerList = towers.toArray(towerList);
			enemyList = enemies.toArray(enemyList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static String parseFieldName(String s) {
		return s.substring(s.indexOf('_') + 1);
	}

	public static String getStringConstant(String className, String valueName) {
		String value = (String) constantMap.get(className.toUpperCase()
				+ valueName);
		if (value != null)
			return value;
		return (String) constantMap.get(valueName.substring(1));
	}

	public static int getIntConstant(String className, String valueName) {
		Integer value = (Integer) constantMap.get(className.toUpperCase()
				+ valueName);
		if (value != null)
			return value.intValue();
		return ((Integer) constantMap.get(valueName.substring(1))).intValue();
	}

	public static double getDoubleConstant(String className, String valueName) {
		Double value = (Double) constantMap.get(className.toUpperCase()
				+ valueName);
		if (value != null)
			return value.doubleValue();
		return ((Double) constantMap.get(valueName.substring(1))).doubleValue();
	}

	// public static Object getConstant(String className, String valueName) {
	// Object value = null;
	// try {
	// value = GameVars.class.getDeclaredField(
	// className.toUpperCase() + valueName).get(null);
	// if (value == null)
	// throw new Exception();
	// } catch (Exception e) {
	// try {
	// value = GameVars.class.getDeclaredField("DEFAULT" + valueName)
	// .get(null);
	// } catch (Exception e2) {
	// e2.printStackTrace();
	// }
	// // For Debugging
	// // System.out.println("Value " + className.toUpperCase() + valueName
	// // + " could not be obtained, reverting to default: " + value);
	// }
	// return value;
	// }

	public static String[] getEnemyList() {
		return enemyList;
	}

	public static String[] getTowerList() {
		return towerList;
	}

	private GameToolbox() {

	}

}
