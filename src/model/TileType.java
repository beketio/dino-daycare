package model;

public enum TileType{
	none(false, false), enemyOnly(false, true), towerOnly(true, false), both;

	private boolean canBuild = true, canPass = true;

	private TileType() {
	}

	private TileType(boolean canBuild, boolean canPass) {
		this.canBuild = canBuild;
		this.canPass = canPass;
	}

	public boolean canBuild() {
		return canBuild;
	}

	public boolean canPass() {
		return canPass;
	}

}
