package model;

import java.util.ArrayList;
import java.util.HashMap;

import server.GameObjectPacket;
import server.GameServer;
import model.Enemies.Enemy;
import model.Towers.BlockBombarder;
import model.gameObject.GameObject;

public class Map {

	private int width, height;

	private ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	private HashMap<Integer, GameObject> iDMap = new HashMap<Integer, GameObject>();
	private Updateable[] updateableObjects;
	private Drawable[] drawableObjects;
	private GameServer server;
	private boolean multiplayer = false;
	private User user;
	
	public Map(int width, int height) {
		this.width = width;
		this.height = height;
		updateUpdateableObjects();
		updateDrawableObjects();
	}

	public Map(int width, int height, GameServer server) {
		this(width, height);
		this.server = server;
	}

	public void giveUser (User user)
	{
		this.user = user;
	}
	
	public void addGameObject(GameObject gameObject) {
		gameObjects.add(gameObject);
		updateUpdateableObjects();
		updateDrawableObjects();
		if (server != null) {
			Object[] args = { gameObject.getClass(),
					new Integer((int) gameObject.getXpos()),
					new Integer((int) gameObject.getYpos()),
					new Integer(gameObject.getID()) };
			server.executeCommand(GameVars.COMMAND_ADD_OBJECT, "", args);
		}
	}

	public void setID(GameObject gameObject) {
		iDMap.put(gameObject.getID(), gameObject);
	}

	// public void updateID(GameObject gameObject, int oldID) {
	// System.out.println("replaced " + oldID + " with " + gameObject.getID());
	// iDMap.remove(oldID);
	// iDMap.put(gameObject.getID(), gameObject);
	// System.out.println(iDMap);
	// }

	public void removeGameObject(GameObject gameObject) {
		gameObjects.remove(gameObject);
		if (gameObject instanceof Enemy)
		{
			user.setCurrency (user.getCurrency () + gameObject.getCost());
		}
		if (gameObject != null)
			iDMap.remove(gameObject.getID());
		updateUpdateableObjects();
		updateDrawableObjects();
		if (server != null) {
			Object[] args = { new Integer(gameObject.getID()) };
			server.executeCommand(GameVars.COMMAND_REMOVE_OBJECT, "", args);
		}
	}

	public void removeGameObject(int iD) {
		GameObject object = iDMap.get(iD);
		if (object != null)
			object.removeFromMap();
		// removeGameObject(object);
		// for (GameObject ob : gameObjects)
		// if (ob.getID() == iD)
		// removeGameObject(ob);
	}

	public static double getDistance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}

	public GameObject[] getGameObjects() {
		GameObject[] gO = new GameObject[gameObjects.size()];
		return (GameObject[]) gameObjects.toArray(gO);
	}

	public void updatePaths() {

	}

	public void updateDrawableObjects() {
		Drawable[] dO = new Drawable[gameObjects.size()];
		dO = gameObjects.toArray(dO);
		drawableObjects = dO;
	}

	public void updateUpdateableObjects() {
		Updateable[] uO = new Updateable[gameObjects.size()];
		uO = gameObjects.toArray(uO);
		updateableObjects = uO;
	}

	public Updateable[] getUpdateableObjects() {
		return updateableObjects;
	}

	public Drawable[] getDrawableObjects() {
		return drawableObjects;
	}

	/*************************************************************/
	/****************** Getters and Setters **********************/
	/*************************************************************/

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void clearMap() {
		gameObjects.clear();
		updateDrawableObjects();
		updateUpdateableObjects();
	}

	public GameObjectPacket[] getObjectPackets() {
		GameObject[] objects = getGameObjects();
		GameObjectPacket[] objectPackets = new GameObjectPacket[objects.length];
		for (int i = 0; i < objects.length; i++)
			objectPackets[i] = new GameObjectPacket(objects[i].getID(),
					GameToolbox.roundDoubleToInt(objects[i].getXpos()),
					GameToolbox.roundDoubleToInt(objects[i].getYpos()),
					objects[i].getRotation(), objects[i].getCurrentHealth());
		return objectPackets;
	}

	public void update(GameObjectPacket[] objectPackets) {
		// ArrayList<GameObjectPacket> mismatchedID = new
		// ArrayList<GameObjectPacket>();
		// for (int i = 0; i < objectPackets.length; i++) {
		// if (i < gameObjects.size()
		// && objectPackets[i].getID() == gameObjects.get(i).getID())
		// gameObjects.get(i).update(objectPackets[i]);
		// else
		// mismatchedID.add(objectPackets[i]);
		// }
		// if (!mismatchedID.isEmpty()) {
		// System.out.println("ID MISMATCHES:");
		// System.out.println(mismatchedID);
		// }
		// System.out.println(iDMap);
		for (int i = 0; i < objectPackets.length; i++) {
			// System.out.println("Packet ID=" + objectPackets[i].getID());
			GameObject ob = iDMap.get(objectPackets[i].getID());
			// System.out.println("Object=" + ob);
			if (ob != null)
				ob.update(objectPackets[i]);
			// else
			// System.out.println("ERRURS");
		}

	}

}
