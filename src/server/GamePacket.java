package server;

import java.io.Serializable;

public class GamePacket implements Serializable {

	private GameObjectPacket[] objectPackets;

	public GamePacket(GameObjectPacket[] objectPackets) {
		this.objectPackets = objectPackets;
	}

	public GameObjectPacket[] getObjectPackets() {
		return objectPackets;
	}

}
