package server;

public interface GameExecutable {

	void executeCommand(int command);

	void executeCommand(int command, String source);

	void executeCommand(int command, String source, Object[] args);

}
