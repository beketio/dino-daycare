package server;

import java.io.Serializable;

import model.TileType;
import model.Boards.BoardType;

public class BoardInfo implements Serializable {

	private BoardType bt;
	private TileType[][] info;

	public BoardInfo(BoardType bt, TileType[][] info) {
		this.bt = bt;
		this.info = info;
	}

	public TileType[][] getInfo() {
		return info;
	}

	public BoardType getBoardType() {
		return bt;
	}

}
