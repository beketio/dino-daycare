package server;

import java.io.Serializable;

public class GameObjectPacket implements Serializable {
	private int iD;
	private int xpos, ypos;
	private double rotation, health;

	public GameObjectPacket(int iD, int xpos, int ypos, double rotation,
			double health) {
		this.iD = iD;
		this.xpos = xpos;
		this.ypos = ypos;
		this.rotation = rotation;
		this.health = health;
	}

	public int getID() {
		return iD;
	}

	public int getX() {
		return xpos;
	}

	public int getY() {
		return ypos;
	}

	public double getRotation() {
		return rotation;
	}

	public double getHealth() {
		return health;
	}
}