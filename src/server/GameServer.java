package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;

import model.Game;
import model.GameVars;

public class GameServer implements GameExecutable {
	private GameServer server = this;
	private Game game;
	private ServerSocket serverSocket;
	private Socket p1, p2;

	private ObjectOutputStream outputPlayer1;
	private ObjectOutputStream outputPlayer2;

	private Deque<GameCommand> clientCommandQueue;

	private String serverName = "Server at ";

	public GameServer(int port) {
		game = new Game(GameVars.DEFAULT_FRAME_WIDTH,
				GameVars.DEFAULT_FRAME_HEIGHT, this);
		try {
			serverSocket = new ServerSocket(port);
			serverName += serverSocket.getLocalSocketAddress();
			System.out.println(serverName);
		} catch (Exception e) {
			System.err.println("Server not started!");
			e.printStackTrace();
		}

		clientCommandQueue = new ArrayDeque<GameCommand>();
		new ServerThread().start();
	}

	public void executeCommand(int command) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			return;
		}
	}

	@Override
	public void executeCommand(int command, String source) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			return;
		case GameVars.COMMAND_PAUSE:
		case GameVars.COMMAND_RESUME:
		case GameVars.COMMAND_INCREASE_TIMESCALE:
		case GameVars.COMMAND_DECREASE_TIMESCALE:
			game.executeCommand(command, serverName);
			clientCommandQueue.addFirst(new GameCommand(command, serverName));
			break;
		default:
			executeCommand(command);
		}
	}

	@Override
	public void executeCommand(int command, String source, Object[] args) {
		switch (command) {
		case GameVars.COMMAND_DONOTHING:
			return;
		case GameVars.COMMAND_ADD_OBJECT:
			clientCommandQueue.addLast(new GameCommand(command, serverName,
					args));
			return;
		case GameVars.COMMAND_REMOVE_OBJECT:
			clientCommandQueue.addLast(new GameCommand(command, serverName,
					args));
			return;
		case GameVars.COMMAND_PLACE_TOWER:
			game.placeTower((int) args[0], (int) args[1], (String) args[2]);
			// Object[] args =
			// game.executeCommand(command, source, args);
			return;
		default:
			executeCommand(command, source);
		}

	}

	private class ClientThread extends Thread {
		private Socket client;
		private ObjectInputStream input;

		public ClientThread(Socket client) {
			this.client = client;
			try {
				input = new ObjectInputStream(client.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {

			while (true) {
				try {
					Object ob = input.readObject();
					if (ob instanceof GameCommand) {
						((GameCommand) ob).execute(server);
					}
				} catch (Exception e) {
					return;
				}
			}
		}
	}

	private class ServerThread extends Thread {

		@Override
		public void run() {
			try {
				p1 = serverSocket.accept();
				outputPlayer1 = new ObjectOutputStream(p1.getOutputStream());
				new ClientThread(p1).start();
				System.out.println("Player 1 connected");
				p2 = serverSocket.accept();
				outputPlayer2 = new ObjectOutputStream(p2.getOutputStream());
				new ClientThread(p2).start();
				System.out.println("Player 2 connected");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			game.executeCommand(GameVars.COMMAND_START_EASY);
			clientCommandQueue.addLast(new GameCommand(
					GameVars.COMMAND_START_EASY, serverName));
			BoardInfo boardInfo = game.getBoard().getBoardInfo();
			Object[] args = { boardInfo };
			clientCommandQueue.addLast(new GameCommand(
					GameVars.COMMAND_SET_BOARD, serverName, args));
			while (true) {
				try {
					while (!clientCommandQueue.isEmpty()) {
						GameCommand command = clientCommandQueue.pollFirst();
						outputPlayer1.writeObject(command);
						outputPlayer2.writeObject(command);
					}
					GamePacket packet = game.getPacket();
					outputPlayer1.writeObject(packet);
					outputPlayer2.writeObject(packet);

					Thread.sleep(33);
				} catch (Exception e) {
					System.out.println("Someone Disconnected");
					try {
						outputPlayer1.writeObject(new GameCommand(
								GameVars.COMMAND_QUIT_GAME, serverName));
					} catch (IOException e1) {
					}
					try {
						outputPlayer2.writeObject(new GameCommand(
								GameVars.COMMAND_QUIT_GAME, serverName));
					} catch (IOException e1) {
					}
					return;
				}

			}
		}
	}
}
