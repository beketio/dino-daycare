package server;

import java.io.Serializable;

public class GameCommand implements Serializable {

	private int command = 0;
	private String source = null;
	private Object[] args = null;

	public GameCommand(int command) {
		this.command = command;
	}

	public GameCommand(int command, String source) {
		this.command = command;
		this.source = source;
	}

	public GameCommand(int command, String source, Object[] args) {
		this.command = command;
		this.source = source;
		this.args = args;
	}

	public void execute(GameExecutable executeOn) {
		if (source == null)
			executeOn.executeCommand(command);
		else if (args == null)
			executeOn.executeCommand(command, source);
		else
			executeOn.executeCommand(command, source, args);
	}

}
